import { NgModule } from '@angular/core';
import { DoubletapDirective } from './doubletap/doubletap';
@NgModule({
	declarations: [DoubletapDirective],
	imports: [],
	exports: [DoubletapDirective]
})
export class DirectivesModule {}
