import { HistoryInstrumentPage } from './../history-instrument/history-instrument';
import { HistoryConsumablePage } from './../history-consumable/history-consumable';
import { HistoryChemicalPage } from './../history-chemical/history-chemical';
import { HistoryLabsPage } from './../history-labs/history-labs';
import { HistoryPage } from './../history/history';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, App } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-bookcancellation',
  templateUrl: 'bookcancellation.html',
})
export class BookcancellationPage {

  userid: any = '';

  public BOOKTYPE: string;
  public BOOKID: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public http: HttpClient, public alertCtrl: AlertController, public appCtrl: App) {

    this.userid = localStorage.getItem('storedData');

    this.BOOKTYPE = this.navParams.get('BOOKTYPE');
    this.BOOKID = this.navParams.get('BOOKID');

    let urlbook = 'https://www.unisza.edu.my/api2/smartlab/booking/cancel?booktype=' + this.BOOKTYPE + '&bookid=' + this.BOOKID;
    let databook: Observable<any> = this.http.get(urlbook);
    databook.subscribe(result => {
      console.log(result);
    });

    /* if(this.BOOKTYPE == "1"){
      this.appCtrl.getRootNav().setRoot(HistoryLabsPage);
    }else if(this.BOOKTYPE == "2"){
      this.appCtrl.getRootNav().setRoot(HistoryChemicalPage);
    } else if (this.BOOKTYPE == "3") {
      this.appCtrl.getRootNav().setRoot(HistoryConsumablePage);
    } else if (this.BOOKTYPE == "4") {
      this.appCtrl.getRootNav().setRoot(HistoryInstrumentPage);
    } */

    this.appCtrl.getRootNav().setRoot(HistoryPage);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookcancellationPage');
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

}
