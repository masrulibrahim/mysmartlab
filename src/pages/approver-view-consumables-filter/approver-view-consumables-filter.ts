import { HttpClient } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { AlertController, NavController, NavParams, ViewController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';

/**
 * Generated class for the ApproverViewConsumablesFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

interface Option {
  id: number,
  name: string
}

interface OptionsResponse {
  success: boolean,
  message: string,
  options: Option[]
}

@Component({
  selector: 'page-approver-view-consumables-filter',
  templateUrl: 'approver-view-consumables-filter.html',
})
export class ApproverViewConsumablesFilterPage {

  @ViewChild('store') store;
  @ViewChild('item') item;

  getOptionsSubs: Subscription;
  getStoreOptionsSubs: Subscription;
  getItemOptionSubs: Subscription;

  bookingStatusOptions: Option[];
  storeOptions: Option[];
  itemOptions: Option[];

  bookingStatusIsLoading = false;
  storeIsLoading = false;
  itemIsLoading = false;

  userId: string;
  filterBookingStatus: number;
  filterStore: Option;
  filterItem: Option;
  filterNationalId: string;
  startDate = '';
  endDate = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private http: HttpClient,
    private alertCtrl: AlertController
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApproverViewConsumablesFilterPage');
    this.userId = this.navParams.get('userId');
    if (this.navParams.get('isFilter')) {
      let data = this.navParams.get('data');
      this.filterBookingStatus = data.status;
      this.filterStore = data.store;
      this.filterItem = data.item;
      this.filterNationalId = data.nationalId;
      this.startDate = data.startDate;
      this.endDate = data.endDate;
    }
    this.getBookingStatusOptions();
    this.getStoreOptions();
    this.getItemOptions();
  }

  dismissModal() {
    this.viewCtrl.dismiss({"search": false});
  }

  getBookingStatusOptions() {
    this.bookingStatusIsLoading = true;
    let storeId = this.filterStore ? (this.filterStore.id ? this.filterStore.id : '') : '';
    let itemId = this.filterItem ? (this.filterItem.id ? this.filterItem.id : '') : '';
    this.getOptionsSubs = this.http.get<OptionsResponse>(`https://www.unisza.edu.my/api2/smartlab/consumable/booking/options?category=status&userId=${this.userId}&storeId=${storeId}&itemId=${itemId}&startDate=${this.startDate}&endDate=${this.endDate}`).subscribe(response => {
      this.bookingStatusIsLoading = false;
      if (response.success) {
        this.bookingStatusOptions = response.options;
        console.log(this.bookingStatusOptions);
        if (this.getOptionsSubs) {
          this.getOptionsSubs.unsubscribe();
        }
      } else {
        this.displayAlert();
      }
    }, err => {
      this.bookingStatusIsLoading = false;
      this.displayAlert();
    });
  }

  getStoreOptions() {
    this.storeIsLoading = true;
    let bookingStatus = this.filterBookingStatus ? this.filterBookingStatus : '';
    let itemId = this.filterItem ? (this.filterItem.id ? this.filterItem.id : '') : '';
    this.getStoreOptionsSubs = this.http.get<OptionsResponse>(`https://www.unisza.edu.my/api2/smartlab/consumable/booking/options?category=store&userId=${this.userId}&bookingStatus=${bookingStatus}&itemId=${itemId}&startDate=${this.startDate}&endDate=${this.endDate}`).subscribe(response => {
      this.storeIsLoading = false;
      if (response.success) {
        this.storeOptions = response.options;
        console.log(this.storeOptions);
        if (this.getStoreOptionsSubs) {
          this.getStoreOptionsSubs.unsubscribe();
        }
      } else {
        this.displayAlert();
      }
    });
  }

  getItemOptions() {
    this.itemIsLoading = true;
    let bookingStatus = this.filterBookingStatus ? this.filterBookingStatus : '';
    let storeId = this.filterStore ? (this.filterStore.id ? this.filterStore.id : '') : '';
    this.getItemOptionSubs = this.http.get<OptionsResponse>(`https://www.unisza.edu.my/api2/smartlab/consumable/booking/options?category=item&userId=${this.userId}&bookingStatus=${bookingStatus}&storeId=${storeId}&startDate=${this.startDate}&endDate=${this.endDate}`).subscribe(response => {
      this.itemIsLoading = false;
      if (response.success) {
        this.itemOptions = response.options;
        console.log(this.itemOptions);
        if (this.getItemOptionSubs) {
          this.getItemOptionSubs.unsubscribe();
        }
      } else {
        this.displayAlert();
      }
    });
  }

  displayAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Sorry!',
      message: 'An unexpected error occured. Please try again later.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    if (this.getOptionsSubs) {
      this.getOptionsSubs.unsubscribe();
    }
    if (this.getStoreOptionsSubs) {
      this.getStoreOptionsSubs.unsubscribe();
    }
    if (this.getItemOptionSubs) {
      this.getItemOptionSubs.unsubscribe();
    }
    confirm.present();
  }
  
  filterBookingStatusUpdated() {
    console.log(this.filterBookingStatus);
    this.getStoreOptions();
    this.getItemOptions();
  }

  filterStoreUpdated() {
    console.log(this.filterStore);
    this.getBookingStatusOptions();
    this.getItemOptions();
  }

  filterItemUpdated() {
    console.log(this.filterItem);
    this.getBookingStatusOptions();
    this.getStoreOptions();
  }

  startDateChanged() {
    let startDate = new Date(this.startDate);

    if (this.endDate) {
      let endDate = new Date(this.endDate);
      if (endDate.getTime() < startDate.getTime()) {
        this.endDate = '';
      }
    }

    this.getBookingStatusOptions();
    this.getItemOptions();
    this.getStoreOptions();
  }

  endDateChanged() {
    this.getBookingStatusOptions();
    this.getItemOptions();
    this.getStoreOptions();
  }

  clearInput() {
    this.store.clear();
    this.item.clear();
    this.filterBookingStatus = null;
    this.filterStore = null;
    this.filterItem = null;
    this.filterNationalId = null;
    this.startDate = '';
    this.endDate = '';
    console.log(this.filterBookingStatus);
    console.log(this.filterStore);
    console.log(this.filterItem);
    this.getBookingStatusOptions();
    this.getStoreOptions();
    this.getItemOptions();
  }

  search() {
    let status = this.filterBookingStatus ? this.filterBookingStatus : '';
    let store = this.filterStore ? this.filterStore : {id: "", name: ""};
    let item = this.filterItem ? this.filterItem : {id: "", name: ""};
    let nationalId = this.filterNationalId ? this.filterNationalId : '';
    let startDate = this.startDate ? this.startDate : '';
    let endDate = this.endDate ? this.endDate : '';
    this.viewCtrl.dismiss({
      "search": true,
      "data": {
        "status": status,
        "store": store,
        "item": item,
        "nationalId": nationalId,
        "startDate": startDate,
        "endDate": endDate
      }
    });
  }

}
