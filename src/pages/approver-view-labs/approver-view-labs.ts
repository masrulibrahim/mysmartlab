import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { AlertController, ModalController, NavController, NavParams } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { ApproverViewLabDetailPage } from '../approver-view-lab-detail/approver-view-lab-detail';
import { ApproverViewLabsFilterPage } from '../approver-view-labs-filter/approver-view-labs-filter';

/**
 * Generated class for the ApproverViewLabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

interface Requester {
  id: string,
  category: string,
  name: string,
  contactNumber: string,
  agency: string
}

interface PIC {
  id: string,
  name: string,
  contactNumber: string
}

interface Approver extends PIC {
  notes: string
}

export interface Lab {
  name: string,
  category: {
    name: string,
    campus: {
      code: string,
      name: string
    },
    status: string
  },
  roomNumber: string,
  officers: PIC[],
  assistantOfficers: PIC[],
  assistants: PIC[]
}

export interface Booking {
  booking: {
    id: number,
    date: string,
    applicationDate: string,
    processedDate: string,
    session: string,
    status: string
  },
  requester: Requester,
  lab: Lab,
  approver: Approver
}

interface GetBookingsResponse {
  success: boolean,
  message: string,
  bookings: Booking[],
  page: number,
  size: number,
  offset: number,
  totalPages: number
}

@Component({
  selector: 'page-approver-view-labs',
  templateUrl: 'approver-view-labs.html',
})
export class ApproverViewLabsPage {

  bookings: Booking[] = [];
  
  getBookingsSubs: Subscription;

  userId: string;
  page: number;
  size: number;
  totalPages: number;

  isLoading = false;

  statusIdFilter = '';
  labFilter = {
    id: "",
    name: ""
  };
  nationalIdFilter = '';
  startDateFilter = '';
  endDateFilter = '';

  isFilter = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: HttpClient,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApproverViewLabsPage');
    this.userId = localStorage.getItem('storedData');
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter ApproverViewLabsPage');
    this.bookings = [];
    this.page = 1;
    this.size = 15;
    this.getBookings();
  }

  getBookings(infiniteScroll?) {
    this.isLoading = true;
    this.getBookingsSubs = this.http.get<GetBookingsResponse>(`https://www.unisza.edu.my/api2/smartlab/approver/view/labs?userid=${this.userId}&page=${this.page}&size=${this.size}&statusId=${this.statusIdFilter}&labId=${this.labFilter.id}&nationalId=${this.nationalIdFilter}&startDate=${this.startDateFilter}&endDate=${this.endDateFilter}`).subscribe(response => {
    
      this.isLoading = false;

      if (infiniteScroll) {
        infiniteScroll.complete();
      }

      if (response.success) {
        this.bookings = this.bookings.concat(response.bookings);
        this.page = response.page;
        this.size = response.size;
        this.totalPages = response.totalPages;
        console.log("Bookings: ", this.bookings);
      }

      if (this.getBookingsSubs) {
        this.getBookingsSubs.unsubscribe();
      }
    }, err => {
      this.displayAlert();
    });
  }

  getMoreBookings(infiniteScroll) {
    this.page++;
    if (this.page > this.totalPages) {
      infiniteScroll.complete();
    } else {
      this.getBookings(infiniteScroll);
    }
  }

  displayAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Sorry!',
      message: 'An unexpected error occured. Please try again later.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    if (this.getBookingsSubs) {
      this.getBookingsSubs.unsubscribe();
    }
    confirm.present();
  }

  getStatusClass(code: string) {
    let result = '';
    switch(code) {
      case 'PENDING':
        result = 'status-pending';
        break;
      case 'APPROVED':
        result = 'status-approved';
        break;
      case 'REJECTED':
      case 'CANCELLED BY ADMIN':
      case 'CANCELLED BY APPLICANT':
        result = 'status-rejected';
        break;
      case 'FULL':
        result = '';
        break;
    }
    return result;
  }

  bookingDetails(booking: Booking) {
    console.log(booking);
    this.navCtrl.push(ApproverViewLabDetailPage, {booking: booking});
  }

  openModal() {
    let filterModal = this.modalCtrl.create(ApproverViewLabsFilterPage, { 
      userId: this.userId,
      isFilter: this.isFilter,
      data: {
        status: this.statusIdFilter,
        lab: this.labFilter,
        nationalId: this.nationalIdFilter,
        startDate: this.startDateFilter,
        endDate: this.endDateFilter
      }
    });
    filterModal.onDidDismiss(response => {
      if (response.search) {
        console.log(response.data);
        this.statusIdFilter = response.data.status;
        this.labFilter = response.data.lab;
        this.nationalIdFilter = response.data.nationalId;
        this.startDateFilter = response.data.startDate;
        this.endDateFilter = response.data.endDate;

        if (this.statusIdFilter || this.labFilter.id || this.nationalIdFilter || this.startDateFilter || this.endDateFilter) {
          this.isFilter = true;
          console.log("Filtering enabled");
        } else {
          this.isFilter = false;
        }

        this.bookings = [];
        this.page = 1;
        this.size = 15;
        this.getBookings();

      }
    });
    filterModal.present();
  }

  translateStatusCode(code: number) {
    let result = '';
    switch(code) {
      case 1:
        result = 'PENDING';
        break;
      case 2:
        result = 'APPROVED';
        break;
      case 3:
        result = 'REJECTED';
        break;
      case 4:
        result = 'FULL';
        break;
      case 5:
        result = 'CANCELLED BY APPLICANT';
        break;
      case 6:
        result = 'CANCELLED BY ADMIN';
        break;
    }
    return result;
  }

}
