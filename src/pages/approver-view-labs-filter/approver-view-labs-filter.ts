import { HttpClient } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { AlertController, NavController, NavParams, ViewController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';

interface Option {
  id: number,
  name: string
}

interface OptionsResponse {
  success: boolean,
  message: string,
  options: Option[]
}

/**
 * Generated class for the ApproverViewLabsFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-approver-view-labs-filter',
  templateUrl: 'approver-view-labs-filter.html',
})
export class ApproverViewLabsFilterPage {

  @ViewChild('lab') lab;

  getOptionsSubs: Subscription;
  getLabOptionsSubs: Subscription;

  bookingStatusOptions: Option[];
  labOptions: Option[];

  bookingStatusIsLoading = false;
  labIsLoading = false;

  userId: string;
  filterBookingStatus: number;
  filterLab: Option;
  filterNationalId: string;
  startDate = '';
  endDate = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private http: HttpClient,
    private alertCtrl: AlertController
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApproverViewLabsFilterPage');
    this.userId = this.navParams.get('userId');
    if (this.navParams.get('isFilter')) {
      let data = this.navParams.get('data');
      this.filterBookingStatus = data.status;
      this.filterLab = data.lab;
      this.filterNationalId = data.nationalId;
      this.startDate = data.startDate;
      this.endDate = data.endDate;
    }
    this.getBookingStatusOptions();
    this.getLabOptions();
  }

  dismissModal() {
    this.viewCtrl.dismiss({"search": false});
  }

  getBookingStatusOptions() {
    this.bookingStatusIsLoading = true;
    let labId = this.filterLab ? (this.filterLab.id ? this.filterLab.id : '') : '';
    console.log(`https://www.unisza.edu.my/api2/smartlab/lab/booking/options?category=status&userId=${this.userId}&labId=${labId}&startDate=${this.startDate}&endDate=${this.endDate}`);
    this.getOptionsSubs = this.http.get<OptionsResponse>(`https://www.unisza.edu.my/api2/smartlab/lab/booking/options?category=status&userId=${this.userId}&labId=${labId}&startDate=${this.startDate}&endDate=${this.endDate}`).subscribe(response => {
      this.bookingStatusIsLoading = false;
      if (response.success) {
        this.bookingStatusOptions = response.options;
        console.log(this.bookingStatusOptions);
        if (this.getOptionsSubs) {
          this.getOptionsSubs.unsubscribe();
        }
      } else {
        this.displayAlert();
      }
    }, err => {
      this.bookingStatusIsLoading = false;
      this.displayAlert();
    });
  }

  getLabOptions() {
    this.labIsLoading = true;
    let bookingStatus = this.filterBookingStatus ? this.filterBookingStatus : '';
    console.log(`https://www.unisza.edu.my/api2/smartlab/lab/booking/options?category=lab&userId=${this.userId}&bookingStatus=${bookingStatus}&startDate=${this.startDate}&endDate=${this.endDate}`);
    this.getLabOptionsSubs = this.http.get<OptionsResponse>(`https://www.unisza.edu.my/api2/smartlab/lab/booking/options?category=lab&userId=${this.userId}&bookingStatus=${bookingStatus}&startDate=${this.startDate}&endDate=${this.endDate}`).subscribe(response => {
      this.labIsLoading = false;
      if (response.success) {
        this.labOptions = response.options;
        console.log(this.labOptions);
        if (this.getLabOptionsSubs) {
          this.getLabOptionsSubs.unsubscribe();
        }
      } else {
        this.displayAlert();
      }
    });
  }

  displayAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Sorry!',
      message: 'An unexpected error occured. Please try again later.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    if (this.getOptionsSubs) {
      this.getOptionsSubs.unsubscribe();
    }
    if (this.getLabOptionsSubs) {
      this.getLabOptionsSubs.unsubscribe();
    }
    confirm.present();
  }
  
  filterBookingStatusUpdated() {
    console.log(this.filterBookingStatus);
    this.getLabOptions();
  }

  filterLabUpdated() {
    console.log(this.filterLab);
    this.getBookingStatusOptions();
  }

  startDateChanged() {
    let startDate = new Date(this.startDate);

    if (this.endDate) {
      let endDate = new Date(this.endDate);
      if (endDate.getTime() < startDate.getTime()) {
        this.endDate = '';
      }
    }

    this.getLabOptions();
    this.getBookingStatusOptions();
  }

  endDateChanged() {
    this.getLabOptions();
    this.getBookingStatusOptions();
  }

  clearInput() {
    this.lab.clear();
    this.filterBookingStatus = null;
    this.filterLab = null;
    this.filterNationalId = null;
    this.startDate = '';
    this.endDate = '';
    console.log(this.filterBookingStatus);
    console.log(this.filterLab);
    this.getBookingStatusOptions();
    this.getLabOptions();
  }

  search() {
    let status = this.filterBookingStatus ? this.filterBookingStatus : '';
    let lab = this.filterLab ? this.filterLab : {id: "", name: ""};
    let nationalId = this.filterNationalId ? this.filterNationalId : '';
    let startDate = this.startDate ? this.startDate : '';
    let endDate = this.endDate ? this.endDate : '';
    this.viewCtrl.dismiss({
      "search": true,
      "data": {
        "status": status,
        "lab": lab,
        "nationalId": nationalId,
        "startDate": startDate,
        "endDate": endDate
      }
    });
  }

}
