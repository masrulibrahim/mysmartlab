import { ComplaintPage } from './../complaint/complaint';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, App, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-complaint-modal',
  templateUrl: 'complaint-modal.html',
})
export class ComplaintModalPage {

  @ViewChild('myreport') myreport;
  @ViewChild('eequipment') eequipment;
  @ViewChild('econsumable') econsumable;

  userid: any = '';

  public CAT: string;
  public LABID: string;

  public equips: any = [];
  public cons: any = [];

  plattype: any;

  // ................................................................................................

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public http: HttpClient, public alertCtrl: AlertController, public appCtrl: App, private plt: Platform) {

    this.userid = localStorage.getItem('storedData');

    this.CAT = this.navParams.get('CAT');
    this.LABID = this.navParams.get('LABID');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  // ................................................................................................

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComplaintModalPage');
  }

  // ................................................................................................

  getEquipment() {

    let urlequip = 'https://www.unisza.edu.my/api2/smartlab/report/equipmentlist?location=' + this.LABID;
    let dataequip: Observable<any> = this.http.get(urlequip);
    dataequip.subscribe(result => {
      this.equips = result;
    });

  }

  // ................................................................................................

  getConsumelist() {

    let urlcon = 'https://www.unisza.edu.my/api2/smartlab/report/consumelist?labid=' + this.LABID;
    let datacon: Observable<any> = this.http.get(urlcon);
    datacon.subscribe(result => {
      this.cons = result;
    });

  }

  // ................................................................................................

  sendReport() {

    let nilai = {

      myreport: this.myreport.value

    };

    let urlreport = 'https://www.unisza.edu.my/api2/smartlab/reportlab?userid=' + this.userid + '&myreport=' + nilai.myreport + '&cat=' + this.CAT + '&labid=' + this.LABID;
    let datareport: Observable<any> = this.http.get(urlreport);
    datareport.subscribe(result => { });

    let confirm = this.alertCtrl.create({
      title: 'Your report has been send to Administrator',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.appCtrl.getRootNav().setRoot(ComplaintPage);
          }
        }
      ]
    });
    confirm.present();

  }

  // ................................................................................................

  public closeModal() {
    this.viewCtrl.dismiss();
  }

}
