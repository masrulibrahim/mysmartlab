import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';

@Component({
  selector: 'page-logout',
  templateUrl: 'logout.html',
})
export class LogoutPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public app: App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LogoutPage');

    this.leaveMe();
  }

  leaveMe() {
    localStorage.clear();

    this.app.getRootNav().setRoot(LoginPage);
    window.location.reload()
  }

}
