import { Labsinfo_2Page } from './../labsinfo-2/labsinfo-2';
import { Labsinfo_1Page } from './../labsinfo-1/labsinfo-1';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-labsinfo',
  templateUrl: 'labsinfo.html',
})
export class LabsinfoPage {

  tab1Root: any = Labsinfo_1Page;
  tab2Root: any = Labsinfo_2Page;

  myIndex: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.myIndex = navParams.data.tabIndex || 0;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LabsinfoPage');
  }

}
