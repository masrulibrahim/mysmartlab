import { HomePage } from './../home/home';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, App, Platform, ToastController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  @ViewChild('ename') ename;
  @ViewChild('enokp') enokp;
  @ViewChild('eusername') eusername;
  @ViewChild('epassword') epassword;
  @ViewChild('eaddress') eaddress;
  @ViewChild('enotel') enotel;
  @ViewChild('eemail') eemail;
  @ViewChild('eagency') eagency;
  @ViewChild('eaddress2') eaddress2;
  @ViewChild('eposition') eposition;
  @ViewChild('egred') egred;

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public alertCtrl: AlertController, public app: App, private plt: Platform, public toastCtrl: ToastController) {

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

    this.warning();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  register(){

    let nilai = {

      ename: this.ename.value,
      enokp: this.enokp.value,
      eusername: this.eusername.value,
      epassword: this.epassword.value,
      eaddress: this.eaddress.value,
      enotel: this.enotel.value,
      eemail: this.eemail.value,
      eagency: this.eagency.value,
      eaddress2: this.eaddress2.value,
      eposition: this.eposition.value,
      egred: this.egred.value

    };

    if(nilai.ename=="" || nilai.enokp=="" || nilai.eusername=="" || nilai.epassword=="" || nilai.eaddress=="" || nilai.enotel=="" || nilai.eemail=="" || nilai.eagency=="" || nilai.eaddress2=="" || nilai.eposition=="" || nilai.egred==""){

      let confirm = this.alertCtrl.create({
        title: 'Please fulfill all required fields',
        buttons: [
          {
            text: 'OK'
          }
        ]
      });
      confirm.present();

    }else{

      let urlreg = 'https://www.unisza.edu.my/api2/smartlab/registeruser?nokp=' + nilai.enokp + '&nama=' + nilai.ename + '&username=' + nilai.eusername + '&password=' + nilai.epassword + '&alamat=' + nilai.eaddress + '&notel=' + nilai.enotel + '&email=' + nilai.eemail + '&agensi=' + nilai.eagency + '&alamatagensi=' + nilai.eaddress2 + '&jawatan=' + nilai.eposition + '&gred=' + nilai.egred;
      let datareg: Observable<any> = this.http.get(urlreg);
      datareg.subscribe(result => { });

      let confirm = this.alertCtrl.create({
        title: 'Your registration has been send and will be process, Thank You!',
        buttons: [
          {
            text: 'OK',
            handler: () => {
              this.app.getRootNav().setRoot(HomePage);
            }
          }
        ]
      });
      confirm.present();

    }

  }

  warning(){

    const toast = this.toastCtrl.create({
      message: 'Only outsider guest need to register. Registered student or stuff can contact any Laboratory Person-in-Charge for helping hand.',
      showCloseButton: true,
      closeButtonText: 'X',
      position: 'bottom',
      duration: 15000,
      cssClass: 'info'
    });
    toast.present();

  }

}
