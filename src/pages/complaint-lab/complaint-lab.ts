import { ComplaintModalPage } from './../complaint-modal/complaint-modal';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { IonicSelectableComponent } from 'ionic-selectable';

class Lab {
  public LABID: number;
  public LABNAME: string;
}

@Component({
  selector: 'page-complaint-lab',
  templateUrl: 'complaint-lab.html',
})
export class ComplaintLabPage {

  labs: Lab[];
  lab: Lab;

  @ViewChild('ecampus') ecampus;

  userid: any = '';
  labid: any = '';

  public campuses: any = [];
  public items: any = [];

  plattype: any;

  // ................................................................................................

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public modalCtrl: ModalController, private plt: Platform) {

    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  // ................................................................................................

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComplaintLabPage');
    this.getCampuslist();
  }

  // ................................................................................................

  getCampuslist() {

    let urlcampus = 'https://www.unisza.edu.my/api2/smartlab/campuslist';
    let datacampus: Observable<any> = this.http.get(urlcampus);
    datacampus.subscribe(result => {
      this.campuses = result;
    });

  }

  // ................................................................................................

  getLablist() {

    let nilai = {

      ecampus: this.ecampus.value

    };

    let urlmakmal = 'https://www.unisza.edu.my/api2/smartlab/lablist?campusid=' + nilai.ecampus;
    let datamakmal: Observable<any> = this.http.get(urlmakmal);
    datamakmal.subscribe(result => {
      this.labs = result;
    });

  }

  // ................................................................................................

  portChange(event: { component: IonicSelectableComponent, value: any }) {
    this.labid = event.value.LABID;
    console.log('port:', event.value.LABID);
    this.getComplaintSpec();
  }

  // ................................................................................................

  getComplaintSpec() {

    let urlauth = 'https://www.unisza.edu.my/api2/smartlab/report/authqrcode?labid=' + this.labid;
    let dataauth: Observable<any> = this.http.get(urlauth);
    dataauth.subscribe(result => {
      this.items = result;
    });

  }

  // ................................................................................................

  openModal(getCAT: string, getLABID: string) {

    var modalPage = this.modalCtrl.create(ComplaintModalPage, { CAT: getCAT, LABID: getLABID });
    modalPage.present();

  }

  // ................................................................................................

}
