import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';

declare var google;

@Component({
  selector: 'page-labreview',
  templateUrl: 'labreview.html',
})
export class LabreviewPage {

  imgslider: Observable<any>;
  public items: any;

  public infos: any = [];

  // @ViewChild('map') mapContainer: ElementRef;
  // map: any;

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  public LABID: string;
  public LAT: string;
  public LNG: string;

  plat: any = '';
  latitute: any = '';
  longlitute: any = '';

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public httpClient: HttpClient, public geolocation: Geolocation, private launchNavigator: LaunchNavigator, private plt: Platform) {

    this.LABID = this.navParams.get('LABID');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

    let urlloc = 'https://www.unisza.edu.my/api2/smartlab/info/lablist2?labid=' + this.LABID;
    let dataloc: Observable<any> = this.httpClient.get(urlloc);
    dataloc.subscribe(result => {
      this.latitute = result[0]['LAT'];
      this.longlitute = result[0]['LNG'];

      console.log('latitute:', this.latitute);
      console.log('longlitute:', this.longlitute);

      var urlandroid="geo:?q=" + this.latitute + "," + this.longlitute;
      var urlios="comgooglemaps://?q=" + this.latitute + "," + this.longlitute;

      console.log('urlandroid:', urlandroid);
      console.log('urlios:', urlios);

    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LabreviewPage');
    this.infoSummoning();
    // this.loadMap();
    // this.addMarker();
  }

  loadMap(result) {

    let lat = result[0]['LAT'];
    let lng = result[0]['LNG'];

    console.log('lat:', lat);
    console.log('lng:', lng);

    // let latLng = new google.maps.LatLng(5.323363, 103.150245);
    let latLng = new google.maps.LatLng(lat, lng);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    this.addMarker();

  }

  addMarker() {

    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: this.map.getCenter()
    });

    let content = "<h4>Information!</h4>";

    this.addInfoWindow(marker, content);

  }

  addInfoWindow(marker, content) {

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });

  }

  infoSummoning() {

    let urlinfo = 'https://www.unisza.edu.my/api2/smartlab/info/lablist2?labid=' + this.LABID;
    let datainfo: Observable<any> = this.httpClient.get(urlinfo);
    datainfo.subscribe(result => {
      this.infos = result;
      /* this.LAT = result[0]['LAT'];
      this.LNG = result[0]['LNG']; */
      this.loadMap(result);
    });

  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

  ionViewDidEnter() {
    // this.loadmap();
  }

  navigateLocation(getLAT: number, getLNG: number) {

    let options: LaunchNavigatorOptions = {
      start: [getLAT, getLNG],
      app: this.launchNavigator.APP.GOOGLE_MAPS
    };
    this.launchNavigator.navigate('London, ON', options)
      .then(success => {
        console.log(success);
      }, error => {
        console.log(error);
      })
  }

  /* loadmap(result) {

    this.LAT = result[0]['LAT'];
    this.LNG = result[0]['LNG'];

    this.map = leaflet.map("map").fitWorld();

    leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      maxZoom: 18
    }).addTo(this.map);

    this.map.locate({
      setView: true,
      maxZoom: 10
    }).on('locationfound', (e) => {
      let markerGroup = leaflet.featureGroup();
      let marker: any = leaflet.marker([e.latitude, e.longitude]).on('click', () => {
        alert('Marker clicked');
      })
      markerGroup.addLayer(marker);
      this.map.addLayer(markerGroup);
      this.map.panTo(leaflet.marker(this.LAT, this.LNG));
    }).on('locationerror', (err) => {
      alert(err.message);
    })

  } */

}
