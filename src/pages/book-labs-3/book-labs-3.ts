import { HistoryLabsPage } from './../history-labs/history-labs';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, App, AlertController, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-book-labs-3',
  templateUrl: 'book-labs-3.html',
})
export class BookLabs_3Page {

  @ViewChild('undergone') undergone;
  @ViewChild('cert') cert;

  userid: any = '';
  public LABID: string;
  public TARIKH: string;
  public SESI: string;

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public http: HttpClient, public appCtrl: App, public alertCtrl: AlertController, private plt: Platform) {

    this.userid = localStorage.getItem('storedData');

    this.LABID = this.navParams.get('LABID');
    this.TARIKH = this.navParams.get('TARIKH');
    this.SESI = this.navParams.get('SESI');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookLabs_3Page');
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

  book(){

    let nilai = {
      undergone: this.undergone.value,
      cert: this.cert.value
    }

    console.log('undergone:', nilai.undergone);
    console.log('cert:', nilai.cert);
    console.log('LABID:', this.LABID);
    console.log('TARIKH:', this.TARIKH);
    console.log('SESI:', this.SESI);

    let urlbook = 'https://www.unisza.edu.my/api2/smartlab/bookinglab?userid=' + this.userid + '&labid=' + this.LABID + '&tarikh=' + this.TARIKH + '&sesi=' + this.SESI + '&undergone=' + nilai.undergone + '&cert=' + nilai.cert;
    let databook: Observable<any> = this.http.get(urlbook);
    databook.subscribe(result => { });

    let confirm = this.alertCtrl.create({
      title: 'Your lab booking will be process...',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.appCtrl.getRootNav().setRoot(HistoryLabsPage);
          }
        }
      ]
    });
    confirm.present();

  }

  back(){

    this.navCtrl.pop();

  }

}
