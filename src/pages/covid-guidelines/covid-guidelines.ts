import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';

@Component({
  selector: 'page-covid-guidelines',
  templateUrl: 'covid-guidelines.html',
})
export class CovidGuidelinesPage {

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private plt: Platform) {

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CovidGuidelinesPage');
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

}
