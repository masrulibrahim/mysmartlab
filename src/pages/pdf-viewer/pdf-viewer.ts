import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';

@Component({
  selector: 'page-pdf-viewer',
  templateUrl: 'pdf-viewer.html',
})
export class PdfViewerPage {

  plattype: any;
  displayData: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private plt: Platform) {

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

    this.displayData = this.navParams.get('displayData');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PdfViewerPage');
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

}
