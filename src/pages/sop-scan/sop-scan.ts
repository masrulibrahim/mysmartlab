import { EbookPage } from './../ebook/ebook';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController, Platform } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-sop-scan',
  templateUrl: 'sop-scan.html',
})
export class SopScanPage {

  id: any = '';
  public items: any = [];
  name: any = '';
  url: any = '';

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private barcodeScanner: BarcodeScanner, public http: HttpClient, public modalCtrl: ModalController, public alertCtrl: AlertController, private plt: Platform) {

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SopScanPage');
  }

  scanCode() {

    this.barcodeScanner.scan().then(barcodeData => {
      this.id = barcodeData.text;
      this.getDetail();
    }, (err) => {
      console.log('Error: ', err);
    });

  }

  getDetail(){

    let nilai = {
      theid: this.id
    }

    let urlsop = 'https://www.unisza.edu.my/api2/smartlab/sop?mode=scan&id=' + nilai.theid;
    let datasop: Observable<any> = this.http.get(urlsop);
    datasop.subscribe(result => {
      this.items = result;
    });

  }

  viewPDF(getID: string, getName: string, getURL: string) {

    var modalPage = this.modalCtrl.create(EbookPage, { id: getID, ebookNAME: getName, ebookURL: getURL });
    modalPage.present();

  }

}
