import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-complaint-history',
  templateUrl: 'complaint-history.html',
})
export class ComplaintHistoryPage {

  userid: any = '';

  public complists: any = [];

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, private plt: Platform) {

    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComplaintHistoryPage');
    this.getComplainthistory();
  }

  getComplainthistory() {

    let urlChemlist = 'https://www.unisza.edu.my/api2/smartlab/complaint/history?userid=' + this.userid;
    let dataChemlist: Observable<any> = this.http.get(urlChemlist);
    dataChemlist.subscribe(result => {
      console.log(result);
      this.complists = result;
    });

  }

}
