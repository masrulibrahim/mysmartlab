import { BookPage } from './../book/book';
import { HistoryPage } from './../history/history';
import { ComplaintPage } from './../complaint/complaint';
import { LabsinfoPage } from './../labsinfo/labsinfo';
import { OshaPage } from './../osha/osha';
import { LogoutPage } from './../logout/logout';
import { HelpdeskPage } from './../helpdesk/helpdesk';
import { LoginPage } from './../login/login';
import { MsdsPage } from './../msds/msds';
import { SopInstrumentPage } from './../sop-instrument/sop-instrument';
import { RegisterPage } from './../register/register';
import { SafetymodPage } from './../safetymod/safetymod';
import { ApproverPage } from './../approver/approver';
import { Component } from '@angular/core';
import { NavController, MenuController, ToastController, Platform, AlertController, App } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-quickmenu',
  templateUrl: 'quickmenu.html',
})
export class QuickmenuPage {

  userid: any = '';
  userlevel: any = '';
  adminlevel: any = '';
  namauser: any;
  plattype: any;
  displayname: any;
  public items: any;

  public images: any = [];

  introinfo: Observable<any>;
  nameinfo: Observable<any>;

  getProfileIsLoading = false;
  userLevelIsLoading = false;
  getAdminIsLoading = false;

  constructor(public navCtrl: NavController, public menu: MenuController, public httpClient: HttpClient, public toastCtrl: ToastController, private plt: Platform, private alertCtrl: AlertController, private app: App) {

    this.menu.enable(true);

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

    this.userid = localStorage.getItem('storedData');

    if (this.userid) {
      this.getProfile();
    } else {
      this.getName();
    }

    this.userLevel();
    this.getAdmin();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuickmenuPage');
  }

  pageIsLoading() {
    return this.getProfileIsLoading || this.userLevelIsLoading || this.getAdminIsLoading;
  }

  userLevel() {
    let userLevel = localStorage.getItem('userLevel');
    if (userLevel) {
      console.log('User level already set');
      console.log(userLevel);
      this.userlevel = JSON.parse(userLevel).KAT;
      console.log(this.userlevel);
    } else {
      
      this.userLevelIsLoading = true;
      let urllevel = 'https://www.unisza.edu.my/api2/smartlab/userlevel?userid=' + this.userid;
      let datalevel: Observable<any> = this.httpClient.get(urllevel);
      datalevel.subscribe(result => {
        this.userlevel = result[0]['KAT'];
        localStorage.setItem('userLevel', JSON.stringify(result[0]));
        this.userLevelIsLoading = false;
      });
    }

  }

  getName() {

    this.displayname = "UniSZA smartLAB™ Welcome";

  }

  getProfile() {
    let profile = localStorage.getItem('profile');
    if (profile) {
      console.log('Profile already set');
      this.displayname = JSON.parse(profile).NAMA;
    } else {
      this.getProfileIsLoading = true;
      this.introinfo = this.httpClient.get('https://www.unisza.edu.my/api2/smartlab/introinfo?userid=' + this.userid);
      this.introinfo.subscribe(data => {
  
        console.log('my data: ', data);
        this.displayname = data[0].NAMA;
        localStorage.setItem('profile', JSON.stringify(data[0]));
        // this.userToast(data);
        this.getProfileIsLoading = false;
  
      })
    }

  }

  getAdmin() {
    let adminInfo = localStorage.getItem('adminInfo');
    if (adminInfo) {
      console.log("Admin info already set");
      this.adminlevel = JSON.parse(adminInfo).STATUS;
      console.log(this.adminlevel);
    }
    else {
      this.getAdminIsLoading = true;
      let urladmin = 'https://www.unisza.edu.my/api2/smartlab/admin?userid=' + this.userid;
      let dataadmin: Observable<any> = this.httpClient.get(urladmin);
      dataadmin.subscribe(result => {
        this.adminlevel = result[0]['STATUS'];
        localStorage.setItem('adminInfo', JSON.stringify(result[0]));
        this.getAdminIsLoading = false;
      });
    }

  }

  userToast(data) {

    this.namauser = data[0].NAMA;
    console.log(data[0].NAMA);

    const toast = this.toastCtrl.create({
      message: 'HI, ' + this.namauser,
      showCloseButton: true,
      closeButtonText: 'Ok',
      position: 'bottom',
      duration: 1000,
      cssClass: 'info'
    });
    toast.present();

  }

  book() {

    // this.navCtrl.push(BookPage);
    this.app.getRootNav().setRoot(BookPage);

  }

  history() {

    // this.navCtrl.push(HistoryPage);
    this.app.getRootNav().setRoot(HistoryPage);

  }

  complaint() {

    // this.navCtrl.push(ComplaintPage);
    this.app.getRootNav().setRoot(ComplaintPage);

  }

  info() {

    // this.navCtrl.push(LabsinfoPage);
    this.app.getRootNav().setRoot(LabsinfoPage);

  }

  osha() {

    // this.navCtrl.push(OshaPage);
    this.app.getRootNav().setRoot(OshaPage);

  }

  logout() {

    // this.navCtrl.push(LogoutPage)
    this.app.getRootNav().setRoot(LogoutPage);

  }

  helpdesk() {

    // this.navCtrl.push(HelpdeskPage);
    this.app.getRootNav().setRoot(HelpdeskPage);

  }

  login() {

    // this.navCtrl.push(LoginPage);
    this.app.getRootNav().setRoot(LoginPage);

  }

  msds() {

    // this.navCtrl.push(MsdsPage);
    this.app.getRootNav().setRoot(MsdsPage);

  }

  sop() {

    // this.navCtrl.push(SopInstrumentPage);
    this.app.getRootNav().setRoot(SopInstrumentPage);

  }

  register() {

    // this.navCtrl.push(RegisterPage);
    this.app.getRootNav().setRoot(RegisterPage);

  }

  safetymod() {

    // this.navCtrl.push(SafetymodPage);
    this.app.getRootNav().setRoot(SafetymodPage, { ori:1 });

  }

  approver() {

    // this.navCtrl.push(ApproverPage);
    this.app.getRootNav().setRoot(ApproverPage);

  }

}
