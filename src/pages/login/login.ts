import { RegisterPage } from './../register/register';
import { HomePage } from './../home/home';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, App, AlertController, MenuController, Platform } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  @ViewChild('userid') userid;
  @ViewChild('passwd') passwd;

  loginurl: Observable<any>;

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClient, public app: App, public alertCtrl: AlertController, public menu: MenuController, private plt: Platform) {

    this.menu.enable(false);

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  onLogin() {

    let userId = encodeURIComponent(this.userid.value);
    let password = encodeURIComponent(this.passwd.value);

    this.loginurl = this.httpClient.get('https://www.unisza.edu.my/api2/smartlab/login?userid=' + userId + '&passwd=' + password);
    this.loginurl.subscribe(data => {
      console.log('my data: ', data);
      this.userProcessing(data);
    })

  }

  userProcessing(data) {

    localStorage.clear();

    let userid = data.userid;
    localStorage.setItem('storedData', userid);

    if (data.userid !== 0) {

      this.app.getRootNav().setRoot(HomePage);
      window.location.reload()

    } else {

      let alert = this.alertCtrl.create({
        title: 'invalid USERNAME or PASSWORD!',
        buttons: ['OK']
      });
      alert.present();

    }

  }

  getBack() {

    this.app.getRootNav().setRoot(HomePage);
    window.location.reload()

  }

  register(){

    // this.navCtrl.pop();
    this.app.getRootNav().setRoot(HomePage);

  }

}
