import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-approver-details',
  templateUrl: 'approver-details.html',
})
export class ApproverDetailsPage {

  public externalid: string;
  userid: any = '';
  public users: any = [];

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public viewCtrl: ViewController, private plt: Platform) {

    this.externalid = this.navParams.get('EXTERNALID');
    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApproverDetailsPage');
    this.getExternaldetails();
  }

  getExternaldetails() {

    let urlUser = 'https://www.unisza.edu.my/api2/smartlab/registeruser/new?mod=carian&externalid=' + this.externalid;
    let dataUser: Observable<any> = this.http.get(urlUser);
    dataUser.subscribe(result => {
      this.users = result;
    });

  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

}
