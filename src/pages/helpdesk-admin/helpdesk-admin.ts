import { HelpdeskRespondPage } from './../helpdesk-respond/helpdesk-respond';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-helpdesk-admin',
  templateUrl: 'helpdesk-admin.html',
})
export class HelpdeskAdminPage {

  public items: any;

  searchTerm: string = '';

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public http: HttpClient, private plt: Platform) {

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpdeskAdminPage');
    this.getHelplist();
  }

  getHelplist() {
    let urlhelp = 'https://www.unisza.edu.my/api2/smartlab/helpdesk/list&searchkey=' + this.searchTerm;
    let datahelp: Observable<any> = this.http.get(urlhelp);
    datahelp.subscribe(result => {
      this.items = result;
    });
  }

  filterProcessing(ev: any) {
    this.getHelplist();
    let serVal = ev.target.value;
    if (serVal && serVal.trim() != '') {
      this.items = this.items.filter((item) => {
        return item.NAMA.toUpperCase().indexOf(this.searchTerm.toUpperCase()) > -1;
      })
    }
  }

  respond(getID: string) {

    var modalPage = this.modalCtrl.create(HelpdeskRespondPage, { id: getID });
    modalPage.present();

  }

}
