import { BookcancellationPage } from './../bookcancellation/bookcancellation';
import { HistoryPage } from './../history/history';
import { ApprovingConsumablePage } from './../approving-consumable/approving-consumable';
import { HistoryViewsPage } from './../history-views/history-views';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, App, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { ModalController } from 'ionic-angular';

@Component({
  selector: 'page-history-consumable',
  templateUrl: 'history-consumable.html',
})
export class HistoryConsumablePage {

  userid: any = '';

  public consumelist: any = [];

  plattype: any;

  isLoading = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public modalCtrl: ModalController, public alertCtrl: AlertController, public appCtrl: App, private plt: Platform) {

    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad HistoryConsumablePage');

    this.getConsumelist();

  }

  getConsumelist() {
    this.isLoading = true;
    let urlConsumelist = 'https://www.unisza.edu.my/api2/smartlab/bookinglist/consumable?userid=' + this.userid;
    let dataConsumelist: Observable<any> = this.http.get(urlConsumelist);
    dataConsumelist.subscribe(result => {
      this.isLoading = false;
      if (Array.isArray(result)) {
        this.consumelist = result;
        console.log(this.consumelist);
      }
    });

  }

  openModal(getCAT: string, getITEMNAME: string, getBRAND: string, getCONSUMETYPE: string, getCAMPUS: string, getLAB: string, getROOMNO: string, getKUANTITI: string, getBOOKDATE: string, getAPPLYDATE: string, getOWNER: string, getSTATUS: string, getNOTE: string) {

    var modalPage = this.modalCtrl.create(HistoryViewsPage, { CAT: getCAT, ITEMNAME: getITEMNAME, BRAND: getBRAND, CONSUMETYPE: getCONSUMETYPE, CAMPUS: getCAMPUS, LAB: getLAB, ROOMNO: getROOMNO, KUANTITI: getKUANTITI, BOOKDATE: getBOOKDATE, APPLYDATE: getAPPLYDATE, OWNER: getOWNER, STATUS: getSTATUS, NOTE: getNOTE });
    modalPage.present();

  }

  processConsume(getBOOKID: string) {

    var modalPage = this.modalCtrl.create(ApprovingConsumablePage, { BOOKID: getBOOKID });
    modalPage.present();

  }

  cancelBook(getBOOKTYPE: string, getBOOKID: string) {
    let confirm = this.alertCtrl.create({
      title: 'Cancel Confirmation',
      message: 'Are you sure you want to cancel this booking?',
      buttons: [
        {
          text: 'Back',
          role: 'cancel'
        },
        {
          text: 'Confirm',
          handler: () => {
            this.navCtrl.push(BookcancellationPage, { BOOKTYPE: getBOOKTYPE, BOOKID: getBOOKID });
          }
        }
      ]
    });
    confirm.present();

  }

}
