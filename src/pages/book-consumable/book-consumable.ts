import { BookConsumable_2Page } from './../book-consumable-2/book-consumable-2';
import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams, Platform } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { QuickmenuPage } from '../quickmenu/quickmenu';
import { ApproverViewConsumablesPage } from '../approver-view-consumables/approver-view-consumables';

interface CheckPICResponse {
  success: boolean,
  pic: boolean
}

@Component({
  selector: 'page-book-consumable',
  templateUrl: 'book-consumable.html',
})
export class BookConsumablePage {

  currentEvents = [];

  plattype: any;

  checkDateSubs: Subscription;
  checkPICSubs: Subscription;

  userId: string;

  isPIC = false;

  checkPICIsLoading = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private plt: Platform, private alertCtrl: AlertController, private loadingCtrl: LoadingController, public http: HttpClient) {

    this.userId = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookConsumablePage');
    this.checkPIC();
  }

  onDaySelect(event) {

    let loading = this.loadingCtrl.create({
      content: 'Checking date. Please wait...'
    });

    loading.present();
    let selectedDate = `${event.year}-${event.month + 1}-${event.date}`;

    this.checkDateSubs = this.http.get(`https://www.unisza.edu.my/api2/smartlab/booking/check?tarikh=${selectedDate}&isPIC=${this.isPIC}`).subscribe(async (response: any) => {
      console.log(response);
      if (response.success) {
        await loading.dismiss();
        let myTarikh = {
          hari: event.date,
          bulan: event.month + 1,
          tahun: event.year,
          hari2: event.day
        }
        this.checkDateSubs.unsubscribe();
        this.navCtrl.push(BookConsumable_2Page, { tkh: myTarikh, hari: response.day });
      } else {
        await loading.dismiss();
        let message: string;
        if (response.code === 'BOOKING_DATE_PASSED') {
          message = '<p>Session is unavailable.</p>';
        } else if (response.code === 'BOOKING_DATE_LESS_THAN_THREE_DAYS') {
          message = '<p>You are only allowed to book at least <strong>3 days before</strong> the booking date.</p>';
        } else {
          message = '<p>An unexpected error occured.</p>';
        }
        let confirm = this.alertCtrl.create({
          title: 'Sorry!',
          message: message,
          buttons: [
            {
              text: 'OK',
              role: 'cancel'
            }
          ]
        });
        this.checkDateSubs.unsubscribe();
        confirm.present();
      }
    });

  }

  checkPIC() {
    this.checkPICIsLoading = true;
    this.checkPICSubs = this.http.get<CheckPICResponse>(`https://www.unisza.edu.my/api2/smartlab/check/pic/consumables?userid=${this.userId}`).subscribe(response => {
      this.checkPICIsLoading = false;
      if (response.success) {
        this.isPIC = response.pic;
      } else {
        this.displayAlert();
      }
    }, err => {
      this.checkPICIsLoading = false;
    });
  }

  displayAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Sorry!',
      message: 'An unexpected error occured. Please try again later.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.setRoot(QuickmenuPage);
          }
        }
      ]
    });
    if (this.checkDateSubs) {
      this.checkDateSubs.unsubscribe();
    }
    if (this.checkPICSubs) {
      this.checkPICSubs.unsubscribe();
    }
    confirm.present();
  }

  viewBookings() {
    this.navCtrl.push(ApproverViewConsumablesPage);
  }

}
