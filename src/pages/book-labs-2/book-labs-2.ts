import { HistoryPage } from './../history/history';
import { LabreviewPage } from './../labreview/labreview';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController, App, Platform, LoadingController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { IonicSelectableComponent } from 'ionic-selectable';
import { BookLabs_3Page } from '../book-labs-3/book-labs-3';
import { Subscription } from 'rxjs/Subscription';
import { QuickmenuPage } from '../quickmenu/quickmenu';

class Port {
  public id: number;
  public name: string;
}

class Lab {
  public LABID: number;
  public LABNAME: string;
}

interface LabStatus {
  date: string,
  day: string,
  session: number,
  available: boolean
}

interface LabStatusResponse {
  success: boolean,
  status: LabStatus[],
  code: string
}

interface BookingLabResponse {
  success?: boolean,
  message?: string,
  ok?: boolean,
  result?: any
}

interface BookLabBulkResponse {
  ok: boolean,
  result: any,
  message: string,
  code: string
}

@Component({
  selector: 'page-book-labs-2',
  templateUrl: 'book-labs-2.html',
})
export class BookLabs_2Page {

  ports: Port[];
  port: Port;

  labs: Lab[];
  lab: Lab;

  @ViewChild('ecampus') ecampus;
  @ViewChild('elab') elab;
  @ViewChild('esesi') esesi;
  @ViewChild('proceed') proceed: ElementRef;
  @ViewChild('notAvailable') notAvailable: ElementRef;

  tkh: any;
  hari: any;
  userid: any = '';
  labid: any = '';
  sesi: string = '';
  endTkh: string;
  weekType = "1";
  labStatus: LabStatus[];

  public campuses: any = [];
  // public labs: any = [];
  public sessions: any = [];
  public labstats: any = [];
  public gethari: any = [];

  plattype: any;

  bookingLabSubs: Subscription;
  getLabStatusSubs: Subscription;
  bookLabBulkSubs: Subscription;

  campusIsLoading = false;
  labListIsLoading = false;
  sesiIsLoading = false;
  labStatusIsLoading = false;
  isSearched = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public modalCtrl: ModalController, public alertCtrl: AlertController, public appCtrl: App, private plt: Platform, private loadingCtrl: LoadingController) {

    this.userid = localStorage.getItem('storedData');

    this.tkh = this.navParams.get('tkh');
    this.tkh = this.tkh.tahun + '-' + this.tkh.bulan.toString().padStart(2, 0) + '-' + this.tkh.hari.toString().padStart(2, 0);
    this.endTkh = this.tkh;
    this.hari = this.navParams.get('hari');

    this.ports = [
      { id: 1, name: 'Tokai' },
      { id: 2, name: 'Vladivostok' },
      { id: 3, name: 'Navlakhi' }
    ];

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

    console.log(this.tkh);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookLabs_2Page');

    this.getCampuslist();
  }

  getCampuslist() {

    this.campusIsLoading = true;

    let urlcampus = 'https://www.unisza.edu.my/api2/smartlab/campuslist';
    let datacampus: Observable<any> = this.http.get(urlcampus);
    datacampus.subscribe(result => {
      this.campuses = result;
      this.campusIsLoading = false;
    }, err => {
      this.campusIsLoading = false;
    });

  }

  getLablist() {
    
    this.labListIsLoading = true;

    this.elab.clear();
    this.labs = [];
    this.sessions = [];
    this.sesi = '';
    this.labstats = [];

    let nilai = {

      ecampus: this.ecampus.value

    };

    let urlmakmal = 'https://www.unisza.edu.my/api2/smartlab/lablist?campusid=' + nilai.ecampus;
    let datamakmal: Observable<any> = this.http.get(urlmakmal);
    datamakmal.subscribe(result => {
      this.labs = result;
      this.labListIsLoading = false;
    }, err => {
      this.labListIsLoading = false;
    });

  }

  portChange(event: { component: IonicSelectableComponent, value: any }) {

    this.sessions = [];
    this.sesi = '';
    this.labstats = [];

    this.labid = event.value.LABID;
    console.log('port:', event.value.LABID);
    this.getSesi();
  }

  getSesi() {

    this.sesiIsLoading = true;

    let urlsesi = 'https://www.unisza.edu.my/api2/smartlab/sessionlist';
    let datasesi: Observable<any> = this.http.get(urlsesi);
    datasesi.subscribe(result => {
      this.sessions = result;
      this.sesiIsLoading = false;
    }, err => {
      this.sesiIsLoading = false;
    });

  }

  getLabStatus() {

    if (this.formIncomplete()) {
      return;
    }

    this.labStatusIsLoading = true;

    this.labStatus = [];

    let loading = this.loadingCtrl.create({
      content: 'Searching. Please wait...'
    });
    loading.present();

    this.getLabStatusSubs = this.http.get<LabStatusResponse>(`https://www.unisza.edu.my/api2/smartlab/labstatus?labId=${this.labid}&session=${this.sesi}&startDate=${this.tkh}&endDate=${this.endTkh}&weekType=${this.weekType}`).subscribe(async response => {
      if (response.success) {
        this.labStatus = response.status;
        console.log(this.labStatus);
        setTimeout(async () => {
          await loading.dismiss();
          if (this.labStatus.length > 0) {
            this.proceed.nativeElement.scrollIntoView({behavior: "smooth"});
          } 
          // else {
          //   this.notAvailable.nativeElement.scrollIntoView({behavior: "smooth"});
          // }
        }, 1000);
        this.labStatusIsLoading = false;
        this.isSearched = true;
      } else {
        await loading.dismiss();
        this.labStatusIsLoading = false;
        if (response.code === 'NO_DATES_FOR_SELECTED_WEEK_TYPE') {
          this.isSearched = false;
          let weekType = this.weekType === '1' ? 'weekdays' : (this.weekType === '2' ? 'weekends': '')
          let alert = this.alertCtrl.create({
            title: 'Invalid Input',
            message: `No dates available on ${weekType}`,
            buttons: [
              {
                text: 'OK',
                role: 'cancel'
              }
            ]
          });
          alert.present();
        } else {
          this.displayAlert();
        }
      }
    }, err => {
      this.labStatusIsLoading = false;
      this.displayAlert();
    });

    // let urlstat = 'https://www.unisza.edu.my/api2/smartlab/labstatus?labid=' + this.labid + '&sesi=' + nilai.esesi + '&hari=' + this.hari + '&tarikh=' + this.tkh;
    // let datastat: Observable<any> = this.http.get(urlstat);
    // datastat.subscribe(result => {
    //   this.labstats = result;
    // });

    // console.log(urlstat);

  }

  formIncomplete() {
    return this.labid === '' || this.sesi === '' || this.tkh === '' || this.endTkh === '' || this.weekType === '';
  }

  filterUnavailableOnly() {
    if (this.labStatus) {
      return this.labStatus.filter(status => !status.available);
    }
    return [];
  }

  translateReason(reason: string) {
    if (reason === "CLASS_IN_SESSION") {
      return "Class is in session";
    } else if (reason === "LAB_IS_FULL") {
      return "Lab is full";
    }
    return reason;
  }

  bookingLab() {
    
    let loading = this.loadingCtrl.create({
      content: 'Booking. Please wait...'
    });

    let nilai = { esesi: this.esesi.value };

    loading.present();

    this.bookingLabSubs = this.http.get<BookingLabResponse>(`https://www.unisza.edu.my/api2/smartlab/bookinglab?userid=${this.userid}&labid=${this.labid}&tarikh=${this.tkh}&sesi=${nilai.esesi}`).subscribe(async response => {
      await loading.dismiss();
      if (response.ok) {
        let confirm = this.alertCtrl.create({
          title: 'Booking Successful',
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.appCtrl.getRootNav().setRoot(HistoryPage);
              }
            }
          ]
        });
        confirm.present();
      } else if (!response.success) {
        let confirm = this.alertCtrl.create({
          title: 'Booking Failed',
          message: 'You have already booked this lab.',
          buttons: [
            {
              text: 'OK',
              role: 'cancel'
            }
          ]
        });
        if (this.bookingLabSubs) {
          this.bookingLabSubs.unsubscribe();
        }
        confirm.present();
      } else {
        this.displayAlert();
      }
    }, async err => {
      if (loading) {
        await loading.dismiss();
      }
      this.displayAlert();
    });

    // this.navCtrl.push(BookLabs_3Page, { LABID: this.labid, TARIKH: this.tkh, SESI: nilai.esesi });

  }

  bookLabBulk () {

    let loading = this.loadingCtrl.create({
      content: 'Booking. Please wait...'
    });

    loading.present();

    let bookings = this.labStatus.filter(status => status.available);
    let data = {
      userId: this.userid,
      labId: this.labid,
      startDate: this.tkh,
      endDate: this.endTkh,
      weekType: (this.weekType == '1') ? 'Weekdays': (this.weekType == '2' ? 'Weekends': ''),
      bookings: bookings
    }

    this.bookLabBulkSubs = this.http.post<BookLabBulkResponse>('https://www.unisza.edu.my/api2/smartlab/bookinglab/bulk', data).subscribe(async response => {
      await loading.dismiss();
      if (response.ok) {
        let confirm = this.alertCtrl.create({
          title: 'Booking Successful',
          message: '',
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.appCtrl.getRootNav().setRoot(HistoryPage);
              }
            }
          ]
        });
        confirm.present();
      } else {
        if (response.code === 'BOOKING_ALREADY_EXIST') {
          let confirm = this.alertCtrl.create({
            title: 'Booking Failed',
            message: 'You have already booked all the selected dates.',
            buttons: [
              {
                text: 'OK',
                role: 'cancel'
              }
            ]
          });
          if (this.bookingLabSubs) {
            this.bookingLabSubs.unsubscribe();
          }
          confirm.present();
        }
      }
    }, err => {
      this.displayAlert();
    });
  }

  displayAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Sorry!',
      message: 'An unexpected error occured. Please try again later.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.setRoot(QuickmenuPage);
          }
        }
      ]
    });
    if (this.bookingLabSubs) {
      this.bookingLabSubs.unsubscribe();
    }
    if (this.getLabStatusSubs) {
      this.getLabStatusSubs.unsubscribe();
    }
    confirm.present();
  }

  unavailableBookingExist() {
    return this.filterUnavailableOnly().length > 0
  }

  bookingsAvailable() {
    return this.filterUnavailableOnly().length < this.labStatus.length;
  }

  watchlist() {

    let nilai = { esesi: this.esesi.value };

    let urlbook = 'https://www.unisza.edu.my/api2/smartlab/watchlabs?userid=' + this.userid + '&labid=' + this.labid + '&tarikh=' + this.tkh + '&sesi=' + nilai.esesi;
    let databook: Observable<any> = this.http.get(urlbook);
    databook.subscribe(result => { });

    console.log(urlbook);

    let confirm = this.alertCtrl.create({
      title: 'Watchlist added. Please review time-by-time.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.appCtrl.getRootNav().setRoot(HistoryPage);
          }
        }
      ]
    });
    confirm.present();

  }

  onChange($event) {
    console.log($event);
  }

  backward() {

    // this.navCtrl.push(BookLabsPage);
    this.navCtrl.pop();

  }

  viewLab(getlabid: string) {

    var modalPage = this.modalCtrl.create(LabreviewPage, { LABID: getlabid });
    modalPage.present();

  }

}
