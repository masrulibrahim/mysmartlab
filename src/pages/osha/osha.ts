import { OshaHistoryPage } from './../osha-history/osha-history';
import { OshaReportPage } from './../osha-report/osha-report';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-osha',
  templateUrl: 'osha.html',
})
export class OshaPage {

  tab1Root: any = OshaReportPage;
  tab2Root: any = OshaHistoryPage;

  myIndex: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.myIndex = navParams.data.tabIndex || 0;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OshaPage');
  }

}
