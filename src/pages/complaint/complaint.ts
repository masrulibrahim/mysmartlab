import { ComplaintScanPage } from './../complaint-scan/complaint-scan';
import { ComplaintLabPage } from './../complaint-lab/complaint-lab';
import { ComplaintHistoryPage } from './../complaint-history/complaint-history';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-complaint',
  templateUrl: 'complaint.html',
})
export class ComplaintPage {

  tab1Root: any = ComplaintScanPage;
  tab2Root: any = ComplaintLabPage;
  tab3Root: any = ComplaintHistoryPage;

  myIndex: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.myIndex = navParams.data.tabIndex || 0;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComplaintPage');
  }

}
