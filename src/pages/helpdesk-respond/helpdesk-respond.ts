import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-helpdesk-respond',
  templateUrl: 'helpdesk-respond.html',
})
export class HelpdeskRespondPage {

  @ViewChild('message') message;

  responder: any = '';

  public helps: any = [];
  public userid: string;

  data = { type: '', nickname: '', message: '' };

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public viewCtrl: ViewController, private plt: Platform) {

    this.responder = localStorage.getItem('storedData');
    this.userid = this.navParams.get('id');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpdeskRespondPage');
  }

  closeMe() {
    this.viewCtrl.dismiss();
  }

  sendMessage() {

    let nilai = { message: this.message.value };

    let urlask = 'https://www.unisza.edu.my/api2/smartlab/helpdesk/answer?userid=' + this.userid + '&msg=' + nilai.message + '&responder=' + this.responder;
    let dataask: Observable<any> = this.http.get(urlask);
    dataask.subscribe(result => { });

    this.data.message = '';

    this.ionViewWillEnter();

  }

  ionViewWillEnter() {
    this.myDefaultMethodToFetchData();
  }

  myDefaultMethodToFetchData() {

    let urlhelpdesk = 'https://www.unisza.edu.my/api2/smartlab/helpdesk/get?userid=' + this.userid;
    let datahelpdesk: Observable<any> = this.http.get(urlhelpdesk);
    datahelpdesk.subscribe(result => {
      this.helps = result;
    });

  }

}
