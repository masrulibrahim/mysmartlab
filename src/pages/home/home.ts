import { EventPage } from './../event/event';
import { QuickmenuPage } from './../quickmenu/quickmenu';
import { HelpdeskPage } from './../helpdesk/helpdesk';
import { ApproverPage } from './../approver/approver';
import { SafetymodPage } from './../safetymod/safetymod';
import { RegisterPage } from './../register/register';
import { OshaPage } from './../osha/osha';
import { SopInstrumentPage } from './../sop-instrument/sop-instrument';
import { MsdsPage } from './../msds/msds';
import { LoginPage } from './../login/login';
import { ComplaintPage } from './../complaint/complaint';
import { LogoutPage } from './../logout/logout';
import { LabsinfoPage } from './../labsinfo/labsinfo';
import { HistoryPage } from './../history/history';
import { BookPage } from './../book/book';
import { Component } from '@angular/core';
import { NavController, MenuController, ToastController, Platform, AlertController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  tab1Root: any = QuickmenuPage;
  tab2Root: any = EventPage;

  myIndex: number;

  userid: any = '';
  plattype: any;

  constructor(public navCtrl: NavController, public menu: MenuController, public httpClient: HttpClient, public toastCtrl: ToastController, private plt: Platform, private alertCtrl: AlertController) {

    this.menu.enable(true);

    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    // window.location.reload();
  }

}
