import { HomePage } from './../home/home';
import { ApproverDetailsPage } from './../approver-details/approver-details';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, App, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-approver',
  templateUrl: 'approver.html',
})
export class ApproverPage {

  public users: any = [];
  userid: any = '';

  plattype: any;

  isLoading = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public modalCtrl: ModalController, public alertCtrl: AlertController, public app: App, private plt: Platform) {

    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApproverPage');
    this.getNewuser();
  }

  getNewuser() {

    this.isLoading = true;

    let urlUser = 'https://www.unisza.edu.my/api2/smartlab/registeruser/new';
    let dataUser: Observable<any> = this.http.get(urlUser);
    dataUser.subscribe(result => {
      this.isLoading = false;
      if (Array.isArray(result)) {
        this.users = result;
      }
    }, err => {
      this.isLoading = false;
    });

  }

  banned(getExternalid: string){

    let urlbanned = 'https://www.unisza.edu.my/api2/smartlab/registeruser/process?userid=' + this.userid + '&externalid=' + getExternalid + '&approvalcode=3';
    let databanned: Observable<any> = this.http.get(urlbanned);
    databanned.subscribe(result => { });

    let confirm = this.alertCtrl.create({
      title: 'This user has been banned, thank you!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.app.getRootNav().setRoot(HomePage);
          }
        }
      ]
    });
    confirm.present();

  }

  approve(getExternalid: string){

    let urlapprove = 'https://www.unisza.edu.my/api2/smartlab/registeruser/process?userid=' + this.userid + '&externalid=' + getExternalid + '&approvalcode=2';
    let dataapprove: Observable<any> = this.http.get(urlapprove);
    dataapprove.subscribe(result => { });

    let confirm = this.alertCtrl.create({
      title: 'This user has been approved, thank you!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.app.getRootNav().setRoot(HomePage);
          }
        }
      ]
    });
    confirm.present();

  }

  details(getExternalid: string){
    var modalPage = this.modalCtrl.create(ApproverDetailsPage, { EXTERNALID: getExternalid });
    modalPage.present();
  }

}
