import { HttpClient } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { AlertController, NavController, NavParams, ViewController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';

/**
 * Generated class for the ApproverViewChemicalsFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

interface Option {
  id: number,
  name: string
}

interface OptionsResponse {
  success: boolean,
  message: string,
  options: Option[]
}

@Component({
  selector: 'page-approver-view-chemicals-filter',
  templateUrl: 'approver-view-chemicals-filter.html',
})
export class ApproverViewChemicalsFilterPage {

  @ViewChild('store') store;
  @ViewChild('chemical') chemical;

  getOptionsSubs: Subscription;
  getStoreOptionsSubs: Subscription;
  getChemicalOptionSubs: Subscription;

  bookingStatusOptions: Option[];
  storeOptions: Option[];
  chemicalOptions: Option[];

  bookingStatusIsLoading = false;
  storeIsLoading = false;
  chemicalIsLoading = false;

  userId: string;
  filterBookingStatus: number;
  filterStore: Option;
  filterChemical: Option;
  filterNationalId: string;
  startDate = '';
  endDate = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private http: HttpClient,
    private alertCtrl: AlertController
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApproverViewChemicalsFilterPage');
    this.userId = this.navParams.get('userId');
    if (this.navParams.get('isFilter')) {
      let data = this.navParams.get('data');
      this.filterBookingStatus = data.status;
      this.filterStore = data.store;
      this.filterChemical = data.chemical;
      this.filterNationalId = data.nationalId;
      this.startDate = data.startDate;
      this.endDate = data.endDate;
    }
    this.getBookingStatusOptions();
    this.getStoreOptions();
    this.getChemicalOptions();
  }

  dismissModal() {
    this.viewCtrl.dismiss({"search": false});
  }

  getBookingStatusOptions() {
    this.bookingStatusIsLoading = true;
    let storeId = this.filterStore ? (this.filterStore.id ? this.filterStore.id : '') : '';
    let chemicalId = this.filterChemical ? (this.filterChemical.id ? this.filterChemical.id : '') : '';
    this.getOptionsSubs = this.http.get<OptionsResponse>(`https://www.unisza.edu.my/api2/smartlab/chemical/booking/options?category=status&userId=${this.userId}&storeId=${storeId}&chemicalId=${chemicalId}&startDate=${this.startDate}&endDate=${this.endDate}`).subscribe(response => {
      this.bookingStatusIsLoading = false;
      if (response.success) {
        this.bookingStatusOptions = response.options;
        console.log(this.bookingStatusOptions);
        if (this.getOptionsSubs) {
          this.getOptionsSubs.unsubscribe();
        }
      } else {
        this.displayAlert();
      }
    }, err => {
      this.bookingStatusIsLoading = false;
      this.displayAlert();
    });
  }

  getStoreOptions() {
    this.storeIsLoading = true;
    let bookingStatus = this.filterBookingStatus ? this.filterBookingStatus : '';
    let chemicalId = this.filterChemical ? (this.filterChemical.id ? this.filterChemical.id : '') : '';
    this.getStoreOptionsSubs = this.http.get<OptionsResponse>(`https://www.unisza.edu.my/api2/smartlab/chemical/booking/options?category=store&userId=${this.userId}&bookingStatus=${bookingStatus}&chemicalId=${chemicalId}&startDate=${this.startDate}&endDate=${this.endDate}`).subscribe(response => {
      this.storeIsLoading = false;
      if (response.success) {
        this.storeOptions = response.options;
        console.log(this.storeOptions);
        if (this.getStoreOptionsSubs) {
          this.getStoreOptionsSubs.unsubscribe();
        }
      } else {
        this.displayAlert();
      }
    });
  }

  getChemicalOptions() {
    this.chemicalIsLoading = true;
    let bookingStatus = this.filterBookingStatus ? this.filterBookingStatus : '';
    let storeId = this.filterStore ? (this.filterStore.id ? this.filterStore.id : '') : '';
    this.getChemicalOptionSubs = this.http.get<OptionsResponse>(`https://www.unisza.edu.my/api2/smartlab/chemical/booking/options?category=chemical&userId=${this.userId}&bookingStatus=${bookingStatus}&storeId=${storeId}&startDate=${this.startDate}&endDate=${this.endDate}`).subscribe(response => {
      this.chemicalIsLoading = false;
      if (response.success) {
        this.chemicalOptions = response.options;
        console.log(this.chemicalOptions);
        if (this.getChemicalOptionSubs) {
          this.getChemicalOptionSubs.unsubscribe();
        }
      } else {
        this.displayAlert();
      }
    });
  }

  displayAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Sorry!',
      message: 'An unexpected error occured. Please try again later.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    if (this.getOptionsSubs) {
      this.getOptionsSubs.unsubscribe();
    }
    if (this.getStoreOptionsSubs) {
      this.getStoreOptionsSubs.unsubscribe();
    }
    if (this.getChemicalOptionSubs) {
      this.getChemicalOptionSubs.unsubscribe();
    }
    confirm.present();
  }
  
  filterBookingStatusUpdated() {
    console.log(this.filterBookingStatus);
    this.getStoreOptions();
    this.getChemicalOptions();
  }

  filterStoreUpdated() {
    console.log(this.filterStore);
    this.getBookingStatusOptions();
    this.getChemicalOptions();
  }

  filterChemicalUpdated() {
    console.log(this.filterChemical);
    this.getBookingStatusOptions();
    this.getStoreOptions();
  }

  startDateChanged() {
    let startDate = new Date(this.startDate);

    if (this.endDate) {
      let endDate = new Date(this.endDate);
      if (endDate.getTime() < startDate.getTime()) {
        this.endDate = '';
      }
    }

    this.getBookingStatusOptions();
    this.getChemicalOptions();
    this.getStoreOptions();
  }

  endDateChanged() {
    this.getBookingStatusOptions();
    this.getChemicalOptions();
    this.getStoreOptions();
  }

  clearInput() {
    this.store.clear();
    this.chemical.clear();
    this.filterBookingStatus = null;
    this.filterStore = null;
    this.filterChemical = null;
    this.filterNationalId = null;
    this.startDate = '';
    this.endDate = '';
    console.log(this.filterBookingStatus);
    console.log(this.filterStore);
    console.log(this.filterChemical);
    this.getBookingStatusOptions();
    this.getStoreOptions();
    this.getChemicalOptions();
  }

  search() {
    let status = this.filterBookingStatus ? this.filterBookingStatus : '';
    let store = this.filterStore ? this.filterStore : {id: "", name: ""};
    let chemical = this.filterChemical ? this.filterChemical : {id: "", name: ""};
    let nationalId = this.filterNationalId ? this.filterNationalId : '';
    let startDate = this.startDate ? this.startDate : '';
    let endDate = this.endDate ? this.endDate : '';
    this.viewCtrl.dismiss({
      "search": true,
      "data": {
        "status": status,
        "store": store,
        "chemical": chemical,
        "nationalId": nationalId,
        "startDate": startDate,
        "endDate": endDate
      }
    });
  }

}
