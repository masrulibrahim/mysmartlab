import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { AlertController, IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { Booking } from '../approver-view-instruments/approver-view-instruments';

/**
 * Generated class for the ApproverViewInstrumentsDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

interface ProcessResponse {
  success: boolean,
  message: string,
  code: string
}

@Component({
  selector: 'page-approver-view-instruments-detail',
  templateUrl: 'approver-view-instruments-detail.html',
})
export class ApproverViewInstrumentsDetailPage {

  booking: Booking;

  approverNote: string;
  cancelNote: string;
  userId: string;

  includeApproverNoteInEmail = false;
  includeCancelNoteInEmail = false;

  approveBookingSubs: Subscription;
  cancelBookingSubs: Subscription;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: HttpClient,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApproverViewInstrumentsDetailPage');
    this.booking = this.navParams.get('booking');
    this.userId = localStorage.getItem('storedData');
  }

  getBookingStatusClass(code: string) {
    let result = '';
    switch(code) {
      case 'PENDING':
        result = 'status-pending';
        break;
      case 'APPROVED':
        result = 'status-approved';
        break;
      case 'REJECTED':
      case 'CANCELLED BY ADMIN':
      case 'CANCELLED BY APPLICANT':
        result = 'status-rejected';
        break;
      case 'FULL':
        result = '';
        break;
    }
    return result;
  }

  trimPrefix(value: string, prefix='%_') {
    if (value) {
      return value.replace(prefix, '');
    }
    return '';
  }

  getLabStatusClass(code: string) {
    let result = '';
    switch(code) {
      case 'ACTIVE':
        result = 'status-approved';
        break;
      case 'INACTIVE':
        result = 'status-rejected';
        break;
    }
    return result;
  }

  hasValue(object: any) {
    return !(object && Object.keys(object).length === 0 && object.constructor === Object);
  }

  approveBooking(isApprove: boolean){

    let loadingContent = isApprove ? "Approving. Please wait..." : "Rejecting. Please wait...";

    let loading = this.loadingCtrl.create({
      content: loadingContent
    });

    loading.present();

    let status = isApprove ? 2 : 3;
    let encodedApproverNote = this.approverNote ? encodeURIComponent(this.approverNote) : '';
    let includeNote = this.approverNote ? this.includeApproverNoteInEmail : false;

    this.approveBookingSubs = this.http.get<ProcessResponse>(`https://www.unisza.edu.my/api2/smartlab/approvinginstrument?userid=${this.userId}&bookid=${this.booking.booking.id}&mynote=${encodedApproverNote}&includeNote=${includeNote}&requesterId=${this.trimPrefix(this.booking.requester.id)}&requesterCategory=${this.booking.requester.category}&status=${status}`).subscribe(response => {
      console.log(response);
      let alertTitle = '';
      let alertSubTitle = '';
      if (response.success) {
        alertTitle = isApprove ? "Booking Application Approved" : "Booking Application Rejected";
        alertSubTitle = isApprove ? "This booking application has been approved" : "This booking application has been rejected";
      } else {
        alertTitle = isApprove ? "Failed to Approve Application" : "Failed to Reject Application";
        alertSubTitle = isApprove ? "An error occurred while approving the application. Please try again later." : "An error occurred while rejecting the application. Please try again later.";
      }

      loading.dismiss().then(()=> {
        let alert = this.alertCtrl.create({
          title: alertTitle,
          subTitle: alertSubTitle,
          buttons: [{
            text: "OK",
            handler: () => {
              alert.dismiss().then(() => {
                this.navCtrl.pop();
              });
              return false;
            }
          }]
        });
    
        alert.present();
      });

      if (this.approveBookingSubs) {
        this.approveBookingSubs.unsubscribe();
      }
    }, err => {
      console.log(err);
      loading.dismiss().then(()=> {
        let alert = this.alertCtrl.create({
          title: "Unexpected Error",
          subTitle: "An unexpected error occurred. Please contact SmartLab technical team if the error persist.",
          buttons: [{
            text: "OK",
            handler: () => {
              alert.dismiss().then(() => {
                this.navCtrl.pop();
              });
              return false;
            }
          }]
        });
    
        alert.present();
      });
      if (this.approveBookingSubs) {
        this.approveBookingSubs.unsubscribe();
      }
    });

  }

  cancelBooking(){

    let loading = this.loadingCtrl.create({
      content: "Cancelling. Please wait..."
    });

    loading.present();

    let status = 6;
    let encodedCancelNote = this.cancelNote ? encodeURIComponent(this.cancelNote) : '';
    let includeNote = this.cancelNote ? this.includeCancelNoteInEmail : false;

    this.cancelBookingSubs = this.http.get(`https://www.unisza.edu.my/api2/smartlab/approvinginstrument?userid=${this.userId}&bookid=${this.booking.booking.id}&mynote=${encodedCancelNote}&includeNote=${includeNote}&requesterId=${this.trimPrefix(this.booking.requester.id)}&requesterCategory=${this.booking.requester.category}&status=${status}`).subscribe(response => {
      console.log(response);
      loading.dismiss().then(()=> {
        let alert = this.alertCtrl.create({
          title: "Booking Application Cancelled",
          subTitle: "This booking application has been cancelled",
          buttons: [{
            text: "OK",
            handler: () => {
              alert.dismiss().then(() => {
                this.navCtrl.pop();
              });
              return false;
            }
          }]
        });
    
        alert.present();
      });

      if (this.cancelBookingSubs) {
        this.cancelBookingSubs.unsubscribe();
      }
    }, err => {
      console.log(err);
      loading.dismiss();
      if (this.cancelBookingSubs) {
        this.cancelBookingSubs.unsubscribe();
      }
    });

  }

}
