import { HistoryInstrumentPage } from './../history-instrument/history-instrument';
import { HistoryConsumablePage } from './../history-consumable/history-consumable';
import { HistoryChemicalPage } from './../history-chemical/history-chemical';
import { HistoryLabsPage } from './../history-labs/history-labs';
import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { HttpClient } from '@angular/common/http';
import { QuickmenuPage } from '../quickmenu/quickmenu';

interface CheckIsStaffResponse {
  success: boolean,
  result: boolean,
  message: string
}

@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  tab1Root: any = HistoryLabsPage;
  tab2Root: any = HistoryChemicalPage;
  tab3Root: any = HistoryConsumablePage;
  tab4Root: any = HistoryInstrumentPage;

  myIndex: number;

  getPICLevelSubs: Subscription;
  checkIfStaffSubs: Subscription;

  userId: string;

  userPICLevel: number;
  isStaff = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: HttpClient,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController
  ) {

    this.myIndex = navParams.data.tabIndex || 0;

    this.userId = localStorage.getItem('storedData');
    this.checkIsStaff();
    this.getPICLevel();

  }

  checkIsStaff() {

    let isStaff = localStorage.getItem('isStaff');
    if (isStaff) {
      console.log("isStaff already set");
      this.isStaff = isStaff == 'true';
      console.log(this.isStaff);
      return;
    }

    console.log("isStaff not set");

    let loading = this.loadingCtrl.create({
      content: "Fetching info. Please wait..."
    });
    loading.present();
    this.checkIfStaffSubs = this.http.get<CheckIsStaffResponse>(`https://www.unisza.edu.my/api2/smartlab/check/staff?userId=${this.userId}`).subscribe(response => {
      loading.dismiss();
      if (response.success) {
        this.isStaff = response.result;
        localStorage.setItem('isStaff', this.isStaff.toString());
      }
      else {
        this.displayAlert();
      }
    }, err => {
      loading.dismiss();
      this.displayAlert();
    });
  }

  getPICLevel() {
    this.getPICLevelSubs = this.http.get(`https://www.unisza.edu.my/api2/smartlab/piclevel?userid=${this.userId}`).subscribe((response: {success: boolean, level: number}) => {
      if (response.success) {
        this.userPICLevel = response.level;
      }
      else {
        this.displayAlert();
      }
    }, err => {
      this.displayAlert();
    });
  }

  displayAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Sorry!',
      message: 'An unexpected error occured. Please try again later.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.setRoot(QuickmenuPage);
          }
        }
      ]
    });
    if (this.getPICLevelSubs) {
      this.getPICLevelSubs.unsubscribe();
    }
    confirm.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
  }

}
