import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {

  @ViewChild('komplen') komplen;
  @ViewChild('ecat') ecat;

  userid: any = '';

  sendreport: Observable<any>;
  sendtelegram: Observable<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClient) {

    this.userid = localStorage.getItem('storedData');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportPage');
  }

  sendReport() {

    let nilai = {
      mykomplen: this.komplen.value,
      mycat: this.ecat.value
    };

    console.log(nilai.mykomplen);

    /* this.sendreport = this.httpClient.get('https://www.unisza.edu.my/api2/smartlab/login?userid=' + nilai.mykomplen);
    this.sendreport.subscribe(data => {}) */

    /* this.sendtelegram = this.httpClient.get('https://api.telegram.org/bot922965052:AAE6vuTIlqhv3nmimJ9G9NgeESkis1L2crM/sendMessage?chat_id=-383016429&text=' + nilai.mykomplen);
    this.sendtelegram.subscribe(data => { }) */

    this.sendtelegram = this.httpClient.get('https://www.unisza.edu.my/api2/smartlab/telegramcomplaint?userid=' + this.userid + '&msg=' + nilai.mykomplen + '&cat=' + nilai.mycat);
    this.sendtelegram.subscribe(data => {})

  }

}
