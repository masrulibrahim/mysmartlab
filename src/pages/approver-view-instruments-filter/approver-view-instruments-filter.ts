import { HttpClient } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { AlertController, NavController, NavParams, ViewController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';

/**
 * Generated class for the ApproverViewInstrumentsFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

interface Option {
  id: number,
  name: string
}

interface OptionsResponse {
  success: boolean,
  message: string,
  options: Option[]
}

@Component({
  selector: 'page-approver-view-instruments-filter',
  templateUrl: 'approver-view-instruments-filter.html',
})
export class ApproverViewInstrumentsFilterPage {

  @ViewChild('lab') lab;
  @ViewChild('instrument') instrument;

  getOptionsSubs: Subscription;
  getLabOptionsSubs: Subscription;
  getInstrumentOptionSubs: Subscription;

  bookingStatusOptions: Option[];
  labOptions: Option[];
  instrumentOptions: Option[];

  bookingStatusIsLoading = false;
  labIsLoading = false;
  instrumentIsLoading = false;

  userId: string;
  filterBookingStatus: number;
  filterLab: Option;
  filterInstrument: Option;
  filterNationalId: string;
  startDate = '';
  endDate = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private http: HttpClient,
    private alertCtrl: AlertController
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApproverViewInstrumentsFilterPage');
    this.userId = this.navParams.get('userId');
    if (this.navParams.get('isFilter')) {
      let data = this.navParams.get('data');
      this.filterBookingStatus = data.status;
      this.filterLab = data.lab;
      this.filterInstrument = data.instrument;
      this.filterNationalId = data.nationalId;
      this.startDate = data.startDate;
      this.endDate = data.endDate;
    }
    this.getBookingStatusOptions();
    this.getLabOptions();
    this.getInstrumentOptions();
  }

  dismissModal() {
    this.viewCtrl.dismiss({"search": false});
  }

  getBookingStatusOptions() {
    this.bookingStatusIsLoading = true;
    let labId = this.filterLab ? (this.filterLab.id ? this.filterLab.id : '') : '';
    let instrumentId = this.filterInstrument ? (this.filterInstrument.id ? this.filterInstrument.id : '') : '';
    this.getOptionsSubs = this.http.get<OptionsResponse>(`https://www.unisza.edu.my/api2/smartlab/instrument/booking/options?category=status&userId=${this.userId}&labId=${labId}&instrumentId=${instrumentId}&startDate=${this.startDate}&endDate=${this.endDate}`).subscribe(response => {
      this.bookingStatusIsLoading = false;
      if (response.success) {
        this.bookingStatusOptions = response.options;
        console.log(this.bookingStatusOptions);
        if (this.getOptionsSubs) {
          this.getOptionsSubs.unsubscribe();
        }
      } else {
        this.displayAlert();
      }
    }, err => {
      this.bookingStatusIsLoading = false;
      this.displayAlert();
    });
  }

  getLabOptions() {
    this.labIsLoading = true;
    let bookingStatus = this.filterBookingStatus ? this.filterBookingStatus : '';
    let instrumentId = this.filterInstrument ? (this.filterInstrument.id ? this.filterInstrument.id : '') : '';
    this.getLabOptionsSubs = this.http.get<OptionsResponse>(`https://www.unisza.edu.my/api2/smartlab/instrument/booking/options?category=lab&userId=${this.userId}&bookingStatus=${bookingStatus}&instrumentId=${instrumentId}&startDate=${this.startDate}&endDate=${this.endDate}`).subscribe(response => {
      this.labIsLoading = false;
      if (response.success) {
        this.labOptions = response.options;
        console.log(this.labOptions);
        if (this.getLabOptionsSubs) {
          this.getLabOptionsSubs.unsubscribe();
        }
      } else {
        this.displayAlert();
      }
    });
  }

  getInstrumentOptions() {
    this.instrumentIsLoading = true;
    let bookingStatus = this.filterBookingStatus ? this.filterBookingStatus : '';
    let labId = this.filterLab ? (this.filterLab.id ? this.filterLab.id : '') : '';
    this.getInstrumentOptionSubs = this.http.get<OptionsResponse>(`https://www.unisza.edu.my/api2/smartlab/instrument/booking/options?category=instrument&userId=${this.userId}&bookingStatus=${bookingStatus}&labId=${labId}&startDate=${this.startDate}&endDate=${this.endDate}`).subscribe(response => {
      this.instrumentIsLoading = false;
      if (response.success) {
        this.instrumentOptions = response.options;
        console.log(this.instrumentOptions);
        if (this.getInstrumentOptionSubs) {
          this.getInstrumentOptionSubs.unsubscribe();
        }
      } else {
        this.displayAlert();
      }
    });
  }

  displayAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Sorry!',
      message: 'An unexpected error occured. Please try again later.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    if (this.getOptionsSubs) {
      this.getOptionsSubs.unsubscribe();
    }
    if (this.getLabOptionsSubs) {
      this.getLabOptionsSubs.unsubscribe();
    }
    if (this.getInstrumentOptionSubs) {
      this.getInstrumentOptionSubs.unsubscribe();
    }
    confirm.present();
  }
  
  filterBookingStatusUpdated() {
    console.log(this.filterBookingStatus);
    this.getLabOptions();
    this.getInstrumentOptions();
  }

  filterLabUpdated() {
    console.log(this.filterLab);
    this.getBookingStatusOptions();
    this.getInstrumentOptions();
  }

  filterInstrumentUpdated() {
    console.log(this.filterInstrument);
    this.getBookingStatusOptions();
    this.getLabOptions();
  }

  startDateChanged() {
    let startDate = new Date(this.startDate);

    if (this.endDate) {
      let endDate = new Date(this.endDate);
      if (endDate.getTime() < startDate.getTime()) {
        this.endDate = '';
      }
    }

    this.getBookingStatusOptions();
    this.getInstrumentOptions();
    this.getLabOptions();
  }

  endDateChanged() {
    this.getBookingStatusOptions();
    this.getInstrumentOptions();
    this.getLabOptions();
  }

  clearInput() {
    this.lab.clear();
    this.instrument.clear();
    this.filterBookingStatus = null;
    this.filterLab = null;
    this.filterInstrument = null;
    this.filterNationalId = null;
    this.startDate = '';
    this.endDate = '';
    console.log(this.filterBookingStatus);
    console.log(this.filterLab);
    console.log(this.filterInstrument);
    this.getBookingStatusOptions();
    this.getLabOptions();
    this.getInstrumentOptions();
  }

  search() {
    let status = this.filterBookingStatus ? this.filterBookingStatus : '';
    let lab = this.filterLab ? this.filterLab : {id: "", name: ""};
    let instrument = this.filterInstrument ? this.filterInstrument : {id: "", name: ""};
    let nationalId = this.filterNationalId ? this.filterNationalId : '';
    let startDate = this.startDate ? this.startDate : '';
    let endDate = this.endDate ? this.endDate : '';
    this.viewCtrl.dismiss({
      "search": true,
      "data": {
        "status": status,
        "lab": lab,
        "instrument": instrument,
        "nationalId": nationalId,
        "startDate": startDate,
        "endDate": endDate
      }
    });
  }

}
