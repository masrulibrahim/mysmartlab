import { HistoryPage } from './../history/history';
import { BookcancellationPage } from './../bookcancellation/bookcancellation';
import { ApprovingLabsPage } from './../approving-labs/approving-labs';
import { HistoryViewsPage } from './../history-views/history-views';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, App, Platform, LoadingController, Content } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { ModalController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { QuickmenuPage } from '../quickmenu/quickmenu';


interface BookingLabResponse {
  success?: boolean,
  message?: string,
  ok?: boolean,
  result?: any
}

@Component({
  selector: 'page-history-labs',
  templateUrl: 'history-labs.html',
})
export class HistoryLabsPage {
  @ViewChild(Content) content: Content;

  userid: any = '';

  public booklists: any = [];
  public watches: any = [];

  plattype: any;

  isLoading = false;

  bookingLabSubs: Subscription;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public modalCtrl: ModalController, public alertCtrl: AlertController, public appCtrl: App, private plt: Platform, private loadingCtrl: LoadingController) {

    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad HistoryLabsPage');

    this.getBookinglist();
    this.getWatchlist();

  }

  getBookinglist() {
    this.isLoading = true;
    let urlBooklist = 'https://www.unisza.edu.my/api2/smartlab/bookinglist/labs?userid=' + this.userid;
    let dataBooklist: Observable<any> = this.http.get(urlBooklist);
    dataBooklist.subscribe(result => {
      this.isLoading = false;
      if (Array.isArray(result)) {
        this.booklists = result;
        console.log(this.booklists);
      }
    });

  }

  getWatchlist() {

    let urlWatch = 'https://www.unisza.edu.my/api2/smartlab/bookinglist/watchlabs?userid=' + this.userid;
    let dataWatch: Observable<any> = this.http.get(urlWatch);
    dataWatch.subscribe(result => {
      this.watches = result;
    });

  }

  openModal(
    getCAT: string,
    getLABNAME: string,
    getLABTYPE: string,
    getROOM: string,
    getPEGAWAI1: string,
    getPEGAWAI2: string,
    getPENPEG1: string,
    getPENPEG2: string,
    getPEMMAKMAL1: string,
    getPEMMAKMAL2: string,
    getBOOKDATE: string,
    getAPPLYDATE: string,
    getSESI: string,
    getSTATUS: string,
    getNOTE: string) {

    var modalPage = this.modalCtrl.create(HistoryViewsPage, {
      CAT: getCAT,
      LABNAME: getLABNAME,
      LABTYPE: getLABTYPE,
      ROOM: getROOM,
      PEGAWAI1: getPEGAWAI1,
      PEGAWAI2: getPEGAWAI2,
      PENPEG1: getPENPEG1,
      PENPEG2: getPENPEG2,
      PEMMAKMAL1: getPEMMAKMAL1,
      PEMMAKMAL2: getPEMMAKMAL2,
      BOOKDATE: getBOOKDATE,
      APPLYDATE: getAPPLYDATE,
      SESI: getSESI,
      STATUS: getSTATUS,
      NOTE: getNOTE
    });
    modalPage.present();

  }

  // processLab(getBOOKID: string) {

  //   var modalPage = this.modalCtrl.create(ApprovingLabsPage, { BOOKID: getBOOKID });
  //   modalPage.present();

  // }

  cancelBook(getBOOKTYPE: string, getBOOKID: string) {
    let confirm = this.alertCtrl.create({
      title: 'Cancel Confirmation',
      message: 'Are you sure you want to cancel this booking?',
      buttons: [
        {
          text: 'Back',
          role: 'cancel'
        },
        {
          text: 'Confirm',
          handler: () => {
            this.navCtrl.push(BookcancellationPage, { BOOKTYPE: getBOOKTYPE, BOOKID: getBOOKID });
          }
        }
      ]
    });
    confirm.present();

  }

  bookingLab(getWATCHID: string) {
    
    let loading = this.loadingCtrl.create({
      content: 'Booking. Please wait...'
    });

    loading.present();

    this.bookingLabSubs = this.http.get<BookingLabResponse>(`https://www.unisza.edu.my/api2/smartlab/bookingwatch?watchid=${getWATCHID}`).subscribe(async response => {
      await loading.dismiss();
      if (response.ok) {
        let confirm = this.alertCtrl.create({
          title: 'Booking Successful',
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.appCtrl.getRootNav().setRoot(HistoryPage);
              }
            }
          ]
        });
        confirm.present();
      } else if (!response.success) {
        let confirm = this.alertCtrl.create({
          title: 'Booking Failed',
          message: 'You have already booked this lab.',
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.appCtrl.getRootNav().setRoot(HistoryPage);
              }
            }
          ]
        });
        if (this.bookingLabSubs) {
          this.bookingLabSubs.unsubscribe();
        }
        confirm.present();
      } else {
        this.displayAlert();
      }
    }, async err => {
      if (loading) {
        await loading.dismiss();
      }
      this.displayAlert();
    });

  }

  displayAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Sorry!',
      message: 'An unexpected error occured. Please try again later.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.setRoot(QuickmenuPage);
          }
        }
      ]
    });
    if (this.bookingLabSubs) {
      this.bookingLabSubs.unsubscribe();
    }
    confirm.present();
  }

}
