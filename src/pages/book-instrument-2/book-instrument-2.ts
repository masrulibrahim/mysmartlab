import { HistoryInstrumentPage } from './../history-instrument/history-instrument';
import { BookInstrumentPage } from './../book-instrument/book-instrument';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { IonicSelectableComponent } from 'ionic-selectable';
import { Subscription } from 'rxjs/Subscription';
import { QuickmenuPage } from '../quickmenu/quickmenu';

class Inst {
  public INSTRUMENT: string;
}

interface GetInstrumentsResponse {
  success: boolean,
  message: string,
  data?: Inst[],
  page?: number,
  size?: number,
  offset?: number,
  totalPages?: number
}

interface GetInstrumentListResponse {
  success: boolean,
  message: string,
  data?: {
    ID: number,
    INSTRUMENT: string,
    BARKOD: string,
    LOCATION: string
  }
}

@Component({
  selector: 'page-book-instrument-2',
  templateUrl: 'book-instrument-2.html',
})
export class BookInstrument_2Page {

  insts: Inst[];
  inst: Inst;
  page: number;
  size: number;
  totalPages: number;

  getInstrumentsSubs: Subscription;
  searchInstrumentsSubs: Subscription;
  getInstrumentListSubs: Subscription;

  tkh: any;
  hari: any;
  userid: any = '';
  instrument: any = '';

  plattype: any;
  
  instrumentHasSearched = false;
  
  public instruments: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpClient,
    public alertCtrl: AlertController, 
    private plt: Platform
  ) {

    this.userid = localStorage.getItem('storedData');

    this.tkh = this.navParams.get('tkh');
    this.tkh = this.tkh.tahun + '-' + this.tkh.bulan + '-' + this.tkh.hari;

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }
    console.log("Platform: ", this.plattype);

  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad BookInstrument_2Page');
    this.initInstruments();

  }

  initInstruments(event?: {
    component: IonicSelectableComponent,
    text: string
  }) {

    this.page = 1;
    this.size = 15;

    this.getInstrumentsSubs = this.http.get<GetInstrumentsResponse>(`https://www.unisza.edu.my/api2/smartlab/instrumentlist?page=${this.page}&size=${this.size}`).subscribe(response => {
      if (response.success) {
        if (event) {
          event.component.items = response.data
        } else {
          this.insts = response.data;
        }
        this.page = response.page;
        this.size = response.size;
        this.totalPages = response.totalPages
      } else {
        this.displayAlert();
      }
    }, err => {
      this.displayAlert();
    });
  }

  displayAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Sorry!',
      message: 'An unexpected error occured. Please try again later.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.setRoot(QuickmenuPage);
          }
        }
      ]
    });
    if (this.getInstrumentsSubs) {
      this.getInstrumentsSubs.unsubscribe();
    }
    if (this.searchInstrumentsSubs) {
      this.searchInstrumentsSubs.unsubscribe();
    }
    if (this.getInstrumentListSubs) {
      this.getInstrumentListSubs.unsubscribe();
    }
    confirm.present();
  }

  instrumentChange(event: { component: IonicSelectableComponent, value: any }) {

    this.instrument = event.value.INSTRUMENT;
    console.log('port:', this.instrument);
    this.getInstrumentList();

  }

  getInstrumentList(){

    this.getInstrumentListSubs = this.http.get<GetInstrumentListResponse>(`https://www.unisza.edu.my/api2/smartlab/instrumentlist2?instrument=${this.instrument}&datebook=${this.tkh}`).subscribe(response => {
      this.instrumentHasSearched = true;
      if (response.success) {
        this.instruments = response.data;
      }
      else {
        this.instruments = [];
      }

    }, err => {
      this.displayAlert();
    });

  }

  bookingInstrument(idInst: string) {

    let urlbook = 'https://www.unisza.edu.my/api2/smartlab/bookinginstrument?userid=' + this.userid + '&instrumentid=' + idInst + '&bookdate=' + this.tkh;
    let databook: Observable<any> = this.http.get(urlbook);
    databook.subscribe(result => { });

    let alert = this.alertCtrl.create({
      title: "Successfully Stored!",
      subTitle: "Your instrument booking will be process...",
      buttons: [{
        text: "OK",
        handler: () => {
          alert.dismiss().then(() => {
            this.navCtrl.push(HistoryInstrumentPage, {})
          });
          return false;
        }
      }]
    })

    alert.present();

  }

  backward() {

    this.navCtrl.push(BookInstrumentPage);

  }

  getMoreInstruments(event: {
    component: IonicSelectableComponent,
    text: string
  }) {

    console.log("Getting more instruments");

    let text = (event.text || '').trim().toLowerCase();

    // There're no more instruments - disable infinite scroll.
    this.page++;
    if (this.page > this.totalPages) {
      console.log("No more pages");
      event.component.disableInfiniteScroll();
      return;
    }

    this.getInstrumentsSubs = this.http.get<GetInstrumentsResponse>(`https://www.unisza.edu.my/api2/smartlab/instrumentlist?page=${this.page}&size=${this.size}`).subscribe(response => {
      if (response.success) {
        let insts = event.component.items.concat(response.data);
        this.page = response.page;
        this.size = response.size;
        this.totalPages = response.totalPages

        if (text) {
          insts = this.filterInstruments(insts, text);
        }

        event.component.items = insts;
        event.component.endInfiniteScroll();

        console.log(`${this.page}/${this.totalPages}`);
      } else {
        this.displayAlert();
      }
    }, err => {
      this.displayAlert();
    });
  }

  filterInstruments(instruments: Inst[], text: string) {
    console.log("Filtering Instruments");
    return instruments.filter(instrument => {
      return instrument.INSTRUMENT.toLowerCase().indexOf(text) !== -1;
    });
  }

  searchInstruments(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    console.log("Searching Instruments...");
    let text = event.text.trim().toLowerCase();
    console.log("Text", text);
    event.component.startSearch();

    // Close any running subscription.
    if (this.searchInstrumentsSubs) {
      this.searchInstrumentsSubs.unsubscribe();
    }

    if (!text) {
      // Close any running subscription.
      if (this.searchInstrumentsSubs) {
        this.searchInstrumentsSubs.unsubscribe();
      }

      this.initInstruments(event);
      event.component.endSearch();
      event.component.enableInfiniteScroll();
      return;
    }

    this.searchInstrumentsSubs = this.http.get<GetInstrumentsResponse>('https://www.unisza.edu.my/api2/smartlab/instrumentlist').subscribe(response => {
      
      // Subscription will be closed when unsubscribed manually.
      if (this.searchInstrumentsSubs.closed) {
        return;
      }

      if (response.success) {
        event.component.items = this.filterInstruments(response.data, text);
        event.component.endSearch();
      } else {
        this.displayAlert();
        event.component.endSearch();
      }
    }, err => {
      this.displayAlert();
      event.component.endSearch();
    });
  }

}
