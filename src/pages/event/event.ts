import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-event',
  templateUrl: 'event.html',
})
export class EventPage {

  plattype: any;
  public images: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private plt: Platform, public httpClient: HttpClient) {

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

    this.getEvent();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventPage');
  }

  getEvent() {

    let urlevent = 'https://www.unisza.edu.my/api2/smartlab/labsimages';
    let datasesi: Observable<any> = this.httpClient.get(urlevent);
    datasesi.subscribe(result => {
      this.images = result;
      console.log(this.images);
    });

  }

}
