import { HistoryConsumablePage } from './../history-consumable/history-consumable';
import { BookPage } from './../book/book';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, App, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { IonicSelectableComponent } from 'ionic-selectable';
import { Subscription } from 'rxjs/Subscription';
import { QuickmenuPage } from '../quickmenu/quickmenu';

class Lab {
  public LABID: number;
  public LABNAME: string;
}

class Item {
  public ITEMID: number;
  public ITEMNAME: string;
}

interface GetQuantityResponse {
  success: boolean,
  message: string,
  data: {
    quantity: number
  }
}

@Component({
  selector: 'page-book-consumable-2',
  templateUrl: 'book-consumable-2.html',
})
export class BookConsumable_2Page {

  labs: Lab[];
  lab: Lab;

  items: Item[];
  item: Item;

  @ViewChild('ecampus') ecampus;
  @ViewChild('elab') elab;
  @ViewChild('eitemtype') eitemtype;
  @ViewChild('eitem') eitem;
  @ViewChild('ebil') ebil;

  tkh: any;
  hari: any;
  userid: any = '';
  bilangan: any = '';
  labid: any = '';
  itemid: any = '';
  itemtype: any = '';

  public campuses: any = [];
  // public labs: any = [];
  public itemtypes: any = [];
  // public items: any = [];

  plattype: any;

  getQuantitySubs: Subscription;

  consumableHasSearched = false;

  quantity: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public alertCtrl: AlertController, public appCtrl: App, private plt: Platform) {

    this.userid = localStorage.getItem('storedData');

    this.tkh = this.navParams.get('tkh');
    this.tkh = this.tkh.tahun + '-' + this.tkh.bulan + '-' + this.tkh.hari;

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookConsumable_2Page');
    this.getCampuslist();
  }

  getCampuslist() {

    let urlcampus = 'https://www.unisza.edu.my/api2/smartlab/campuslist';
    let datacampus: Observable<any> = this.http.get(urlcampus);
    datacampus.subscribe(result => {
      if (Array.isArray(result)) {
        this.campuses = result;
      }
    });

  }

  getLablist() {

    this.elab.clear();
    this.itemtypes = [];
    this.itemtype = '';
    this.eitem.clear();
    this.items = [];
    this.bilangan = '';
    this.consumableHasSearched = false;

    let nilai = {

      ecampus: this.ecampus.value

    };

    let urlmakmal = 'https://www.unisza.edu.my/api2/smartlab/lablistitem?campusid=' + nilai.ecampus;
    let datamakmal: Observable<any> = this.http.get(urlmakmal);
    datamakmal.subscribe(result => {
      if (Array.isArray(result)) {
        this.labs = result;
      }
    });

  }

  portChange(event: { component: IonicSelectableComponent, value: any }) {
    
    this.itemtypes = [];
    this.itemtype = '';
    this.eitem.clear();
    this.items = [];
    this.bilangan = '';
    this.consumableHasSearched = false;

    this.labid = event.value.LABID;
    console.log('port:', this.labid);
    this.getItemtype();

  }

  getItemtype() {

    let nilai = {

      ecampus: this.ecampus.value,
      elab: this.elab.value

    };

    let urlitemtype = 'https://www.unisza.edu.my/api2/smartlab/itemtypelist?campusid=' + nilai.ecampus + '&labid=' + this.labid;
    let dataitemtype: Observable<any> = this.http.get(urlitemtype);
    dataitemtype.subscribe(result => {
      if (Array.isArray(result)) {
        this.itemtypes = result;
      }
    });

  }

  getItem() {

    let nilai = {

      ecampus: this.ecampus.value,
      elab: this.elab.value,
      eitemtype: this.itemtype

    };

    let urlitem = 'https://www.unisza.edu.my/api2/smartlab/consumablelist?campusid=' + nilai.ecampus + '&labid=' + this.labid + '&itemtype=' + nilai.eitemtype;
    let dataitem: Observable<any> = this.http.get(urlitem);
    dataitem.subscribe(result => {
      if (Array.isArray(result)) {
        this.items = result;
      }
    });

  }

  portChange2(event: { component: IonicSelectableComponent, value: any }) {

    this.itemid = event.value.ITEMID;
    console.log('port:', this.itemid);
    this.getQuantity();

  }

  getQuantity() {
    
    this.getQuantitySubs = this.http.get<GetQuantityResponse>(`https://www.unisza.edu.my/api2/smartlab/consumablebil?itemid=${this.itemid}`).subscribe(response => {
      this.consumableHasSearched = true;
      if (response.success) {
        this.bilangan = response.data.quantity;
      } else {
        this.bilangan = '';
      }
      console.log(this.bilangan);
      console.log(response);
      console.log(this.itemid);
    }, err => {
      this.displayAlert();
    });

  }

  displayAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Sorry!',
      message: 'An unexpected error occured. Please try again later.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.setRoot(QuickmenuPage);
          }
        }
      ]
    });
    if (this.getQuantitySubs) {
      this.getQuantitySubs.unsubscribe();
    }
    confirm.present();
  }

  backward() {

    this.navCtrl.pop();

  }

  bookingConsum() {

    if (!this.quantity || this.quantity <= 0 || this.quantity > parseFloat(this.bilangan)) {
      console.log("Invalid quantity", this.quantity);
      return;
    }

    let urlbook = 'https://www.unisza.edu.my/api2/smartlab/bookingconsume?userid=' + this.userid + '&consumeid=' + this.itemid + '&bookdate=' + this.tkh + '&bil=' + this.quantity;
    let databook: Observable<any> = this.http.get(urlbook);
    databook.subscribe(result => { });

    let confirm = this.alertCtrl.create({
      title: 'Your consumable booking will be process...',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.appCtrl.getRootNav().setRoot(HistoryConsumablePage);
          }
        }
      ]
    });
    confirm.present();

  }

  quantityChanged() {
    if (!this.isInt(this.quantity)) {
      this.quantity = null;
    } else if (this.quantity > parseFloat(this.bilangan)) {
      this.quantity = parseFloat(this.bilangan);
    } else {
      this.quantity = parseFloat(this.quantity.toString());
    }
  }

  quantityKeyPressed(event) {
    // console.log(event);
    // 45   -> "-"
    // 46   -> "."
    // 101  -> "e"
    if (event.keyCode === 45 || event.keyCode === 46 || event.keyCode === 101) {
      return false;
    } else {
      this.quantityChanged();
    }
  }

  isInt(value) {
    if (isNaN(value)) {
      return false;
    }
    let x = parseFloat(value);
    return (x | 0) === x;
  }

}
