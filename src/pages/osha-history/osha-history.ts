import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-osha-history',
  templateUrl: 'osha-history.html',
})
export class OshaHistoryPage {

  userid: any = '';

  public complists: any = [];

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, private plt: Platform) {

    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OshaHistoryPage');
    this.getOshahistory();
  }

  getOshahistory() {

    let urlChemlist = 'https://www.unisza.edu.my/api2/smartlab/osha/history?userid=' + this.userid;
    let dataChemlist: Observable<any> = this.http.get(urlChemlist);
    dataChemlist.subscribe(result => {
      this.complists = result;
    });

  }

}
