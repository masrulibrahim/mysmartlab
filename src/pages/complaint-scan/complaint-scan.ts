import { ComplaintModalPage } from './../complaint-modal/complaint-modal';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController, Platform } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-complaint-scan',
  templateUrl: 'complaint-scan.html',
})
export class ComplaintScanPage {

  userid: any = '';
  labId = null;
  public items: any = [];

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private barcodeScanner: BarcodeScanner, public http: HttpClient, public modalCtrl: ModalController, public alertCtrl: AlertController, private plt: Platform) {

    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComplaintScanPage');
  }

  scanCode() {

    this.barcodeScanner.scan().then(barcodeData => {
      this.labId = barcodeData.text;
      this.getAuth();
    }, (err) => {
      console.log('Error: ', err);
    });

  }

  getAuth(){

    let nilai = {
      theid: this.labId
    }

    let urlauth = 'https://www.unisza.edu.my/api2/smartlab/report/authqrcode?labid=' + nilai.theid;
    let dataauth: Observable<any> = this.http.get(urlauth);
    dataauth.subscribe(result => {
      this.items = result;
    });

  }

  openModal(getCAT: string, getLABID: string) {

    var modalPage = this.modalCtrl.create(ComplaintModalPage, { CAT: getCAT, LABID: getLABID });
    modalPage.present();

  }

}
