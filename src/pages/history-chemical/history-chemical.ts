import { BookcancellationPage } from './../bookcancellation/bookcancellation';
import { HistoryPage } from './../history/history';
import { ApprovingChemicalPage } from './../approving-chemical/approving-chemical';
import { HistoryViewsPage } from './../history-views/history-views';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, App, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { ModalController } from 'ionic-angular';

@Component({
  selector: 'page-history-chemical',
  templateUrl: 'history-chemical.html',
})
export class HistoryChemicalPage {

  userid: any = '';

  public chemlist: any = [];

  plattype: any;

  isLoading = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public modalCtrl: ModalController, public alertCtrl: AlertController, public appCtrl: App, private plt: Platform) {

    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad HistoryChemicalPage');

    this.getChemlist();

  }

  getChemlist() {
    this.isLoading = true;
    let urlChemlist = 'https://www.unisza.edu.my/api2/smartlab/bookinglist/chemicals?userid=' + this.userid;
    let dataChemlist: Observable<any> = this.http.get(urlChemlist);
    dataChemlist.subscribe(result => {
      this.isLoading = false;
      if (Array.isArray(result)) {
        this.chemlist = result;
        console.log(this.chemlist);
      }
    });

  }

  openModal(getCAT: string, getCHEMICAL: string, getNICKNAME: string, getBRAND: string, getCHEMTYPE: string, getCAMPUS: string, getPTJ: string, getLAB: string, getROOMNO: string, getKUANTITI: string, getBOOKDATE: string, getAPPLYDATE: string, getSESI: string, getSTATUS: string, getNOTE: string) {

    var modalPage = this.modalCtrl.create(HistoryViewsPage, { CAT: getCAT, CHEMICAL: getCHEMICAL, NICKNAME: getNICKNAME, BRAND: getBRAND, CHEMTYPE: getCHEMTYPE, CAMPUS: getCAMPUS, PTJ: getPTJ, LAB: getLAB, ROOMNO: getROOMNO, KUANTITI: getKUANTITI, BOOKDATE: getBOOKDATE, APPLYDATE: getAPPLYDATE, SESI: getSESI, STATUS: getSTATUS, NOTE: getNOTE });
    modalPage.present();

  }

  processLab(getBOOKID: string) {

    var modalPage = this.modalCtrl.create(ApprovingChemicalPage, { BOOKID: getBOOKID });
    modalPage.present();

  }

  cancelBook(getBOOKTYPE: string, getBOOKID: string) {
    let confirm = this.alertCtrl.create({
      title: 'Cancel Confirmation',
      message: 'Are you sure you want to cancel this booking?',
      buttons: [
        {
          text: 'Back',
          role: 'cancel'
        },
        {
          text: 'Confirm',
          handler: () => {
            this.navCtrl.push(BookcancellationPage, { BOOKTYPE: getBOOKTYPE, BOOKID: getBOOKID });
          }
        }
      ]
    });
    confirm.present();

  }

}
