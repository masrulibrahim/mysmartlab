import { HistoryInstrumentPage } from './../history-instrument/history-instrument';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-approving-instrument',
  templateUrl: 'approving-instrument.html',
})
export class ApprovingInstrumentPage {

  @ViewChild('mynote') mynote;

  public BOOKID: string;
  userid: any = '';

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public http: HttpClient, public alertCtrl: AlertController, private plt: Platform) {

    this.BOOKID = this.navParams.get('BOOKID');
    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApprovingInstrumentPage');
  }

  approveBook() {

    let nilai = {

      mynote: this.mynote.value

    };

    let urlapprove = 'https://www.unisza.edu.my/api2/smartlab/approvinginstrument?userid=' + this.userid + '&bookid=' + this.BOOKID + '&mynote=' + nilai.mynote + '&status=2';
    let dataapprove: Observable<any> = this.http.get(urlapprove);
    dataapprove.subscribe(result => { });

    let alert = this.alertCtrl.create({
      title: "Successfully Stored!",
      subTitle: "This booking has successfully approved!",
      buttons: [{
        text: "OK",
        handler: () => {
          alert.dismiss().then(() => {
            this.navCtrl.push(HistoryInstrumentPage, {})
          })
          return false;
        }
      }]
    })

    alert.present();

  }

  declineBook() {

    let nilai = {

      mynote: this.mynote.value

    };

    let urlapprove = 'https://www.unisza.edu.my/api2/smartlab/approvinginstrument?userid=' + this.userid + '&bookid=' + this.BOOKID + '&mynote=' + nilai.mynote + '&status=3';
    let dataapprove: Observable<any> = this.http.get(urlapprove);
    dataapprove.subscribe(result => { });

    let alert = this.alertCtrl.create({
      title: "Successfully Stored!",
      subTitle: "This booking has successfully declined!",
      buttons: [{
        text: "OK",
        handler: () => {
          alert.dismiss().then(() => {
            this.navCtrl.push(HistoryInstrumentPage, {})
          })
          return false;
        }
      }]
    })

    alert.present();

  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

}
