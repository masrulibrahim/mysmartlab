import { HistoryPage } from './../history/history';
import { HistoryLabsPage } from './../history-labs/history-labs';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-approving-labs',
  templateUrl: 'approving-labs.html',
})
export class ApprovingLabsPage {

  @ViewChild('mynote') mynote;

  public BOOKID: string;
  userid: any = '';

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public http: HttpClient, public alertCtrl: AlertController, private plt: Platform) {

    this.BOOKID = this.navParams.get('BOOKID');
    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApprovingLabsPage');
  }

  approveBook(){

    let nilai = {

      mynote: this.mynote.value

    };

    let urlapprove = 'https://www.unisza.edu.my/api2/smartlab/approvinglabs?userid=' + this.userid + '&bookid=' + this.BOOKID + '&mynote=' + nilai.mynote + '&status=2';
    let dataapprove: Observable<any> = this.http.get(urlapprove);
    dataapprove.subscribe(result => { });

    let alert = this.alertCtrl.create({
      title: "Successfully Stored!",
      subTitle: "This booking has successfully approved!",
      buttons: [{
        text: "OK",
        handler: () => {
          alert.dismiss().then(() => {
            this.navCtrl.push(HistoryLabsPage, {})
          })
          return false;
        }
      }]
    })

    alert.present();

  }

  declineBook(){

    let nilai = {

      mynote: this.mynote.value

    };

    let urlapprove = 'https://www.unisza.edu.my/api2/smartlab/approvinglabs?userid=' + this.userid + '&bookid=' + this.BOOKID + '&mynote=' + nilai.mynote + '&status=3';
    let dataapprove: Observable<any> = this.http.get(urlapprove);
    dataapprove.subscribe(result => { });

    let alert = this.alertCtrl.create({
      title: "Successfully Stored!",
      subTitle: "This booking has successfully declined!",
      buttons: [{
        text: "OK",
        handler: () => {
          alert.dismiss().then(() => {
            this.navCtrl.push(HistoryPage, {})
          })
          return false;
        }
      }]
    })

    alert.present();

  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

}
