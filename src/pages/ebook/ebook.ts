import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';

@Component({
  selector: 'page-ebook',
  templateUrl: 'ebook.html',
})
export class EbookPage {

  public ebookURL: string;
  public ebookNAME: string;

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private plt: Platform) {

    this.ebookNAME = this.navParams.get('ebookNAME');
    this.ebookURL = this.navParams.get('ebookURL');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EbookPage');
  }

  closeMe(){
    this.viewCtrl.dismiss();
  }

}
