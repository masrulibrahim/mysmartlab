import { Safetymod1Page } from './../safetymod1/safetymod1';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController, Platform } from 'ionic-angular';

@Component({
  selector: 'page-safetymod',
  templateUrl: 'safetymod.html',
})
export class SafetymodPage {

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public modalCtrl: ModalController, private plt: Platform) {

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SafetymodPage');
  }

  closeMe() {
    this.viewCtrl.dismiss();
  }

  viewModule(){

    var modalPage = this.modalCtrl.create(Safetymod1Page);
    modalPage.present();

  }

}
