import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-history-views',
  templateUrl: 'history-views.html',
})
export class HistoryViewsPage {

  public CAT: string;
  public LABNAME: string;
  public LABTYPE: string;
  public ROOM: string;
  public PEGAWAI: string;
  public PEGAWAI1: string;
  public PEGAWAI2: string;
  public PENPEG1: string;
  public PENPEG2: string;
  public PEMMAKMAL1: string;
  public PEMMAKMAL2: string;
  public BOOKDATE: string;
  public APPLYDATE: string;
  public SESI: string;
  public STATUS: string;
  public NOTE: string;

  public CHEMICAL: string;
  public NICKNAME: string;
  public BRAND: string;
  public CHEMTYPE: string;
  public CAMPUS: string;
  public PTJ: string;
  public LAB: string;
  public ROOMNO: string;
  public KUANTITI: string;

  public INSTRUMENT: string;
  public BARKOD: string;
  public LOCATION: string;

  public ITEMNAME: string;
  public CONSUMETYPE: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {

    this.CAT = this.navParams.get('CAT');
    this.LABNAME = this.navParams.get('LABNAME');
    this.LABTYPE = this.navParams.get('LABTYPE');
    this.ROOM = this.navParams.get('ROOM');
    this.PEGAWAI = this.navParams.get('PEGAWAI');
    this.PEGAWAI1 = this.navParams.get('PEGAWAI1');
    this.PEGAWAI2 = this.navParams.get('PEGAWAI2');
    this.PENPEG1 = this.navParams.get('PENPEG1');
    this.PENPEG2 = this.navParams.get('PENPEG2');
    this.PEMMAKMAL1 = this.navParams.get('PEMMAKMAL1');
    this.PEMMAKMAL2 = this.navParams.get('PEMMAKMAL2');
    this.BOOKDATE = this.navParams.get('BOOKDATE');
    this.APPLYDATE = this.navParams.get('APPLYDATE');
    this.SESI = this.navParams.get('SESI');
    this.STATUS = this.navParams.get('STATUS');
    this.NOTE = this.navParams.get('NOTE');

    this.CHEMICAL = this.navParams.get('CHEMICAL');
    this.NICKNAME = this.navParams.get('NICKNAME');
    this.BRAND = this.navParams.get('BRAND');
    this.CHEMTYPE = this.navParams.get('CHEMTYPE');
    this.CAMPUS = this.navParams.get('CAMPUS');
    this.PTJ = this.navParams.get('PTJ');
    this.LAB = this.navParams.get('LAB');
    this.ROOMNO = this.navParams.get('ROOMNO');
    this.KUANTITI = this.navParams.get('KUANTITI');

    this.INSTRUMENT = this.navParams.get('INSTRUMENT');
    this.BARKOD = this.navParams.get('BARKOD');
    this.LOCATION = this.navParams.get('LOCATION');

    this.ITEMNAME = this.navParams.get('ITEMNAME');
    this.CONSUMETYPE = this.navParams.get('CONSUMETYPE');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryViewsPage');
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

}
