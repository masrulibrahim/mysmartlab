import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { AlertController, ModalController, NavController, NavParams } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { ApproverViewChemicalsDetailPage } from '../approver-view-chemicals-detail/approver-view-chemicals-detail';
import { ApproverViewChemicalsFilterPage } from '../approver-view-chemicals-filter/approver-view-chemicals-filter';
import { Lab } from '../approver-view-labs/approver-view-labs';

interface Requester {
  id: string,
  category: string,
  name: string,
  contactNumber: string,
  agency: string
}

interface PIC {
  id: string,
  name: string,
  contactNumber: string
}

interface Approver extends PIC {
  notes: string
}

interface Chemicals {
  id: number,
  name: string,
  brand: string,
  type: string,
  available: {
    quantity: number,
    unit: string
  }
}

export interface Booking {
  booking: {
    id: number,
    date: string,
    applicationDate: string,
    processedDate: string,
    session: string,
    status: string
  },
  requester: Requester,
  store: Lab,
  chemical: Chemicals,
  approver: Approver
}

interface GetBookingsResponse {
  success: boolean,
  message: string,
  bookings: Booking[],
  page: number,
  size: number,
  offset: number,
  totalPages: number
}

@Component({
  selector: 'page-approver-view-chemicals',
  templateUrl: 'approver-view-chemicals.html',
})
export class ApproverViewChemicalsPage {

  bookings: Booking[] = [];
  
  getBookingsSubs: Subscription;

  userId: string;
  page: number;
  size: number;
  totalPages: number;

  isLoading = false;

  statusIdFilter = '';
  storeFilter = {
    id: '',
    name: ''
  };
  chemicalFilter = {
    id: '',
    name: ''
  };
  nationalIdFilter = '';
  startDateFilter = '';
  endDateFilter = '';

  isFilter = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: HttpClient,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApproverViewChemicalsPage');
    this.userId = localStorage.getItem('storedData');
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter ApproverViewLabsPage');
    this.bookings = [];
    this.page = 1;
    this.size = 7;
    this.getBookings();
  }

  getBookings(infiniteScroll?) {
    this.isLoading = true;
    this.getBookingsSubs = this.http.get<GetBookingsResponse>(`https://www.unisza.edu.my/api2/smartlab/approver/view/chemicals?userid=${this.userId}&page=${this.page}&size=${this.size}&statusId=${this.statusIdFilter}&chemicalId=${this.chemicalFilter.id}&labId=${this.storeFilter.id}&nationalId=${this.nationalIdFilter}&startDate=${this.startDateFilter}&endDate=${this.endDateFilter}`).subscribe(response => {
    
      this.isLoading = false;

      if (infiniteScroll) {
        infiniteScroll.complete();
      }

      if (response.success) {
        this.bookings = this.bookings.concat(response.bookings);
        this.page = response.page;
        this.size = response.size;
        this.totalPages = response.totalPages;
        console.log("Bookings: ", this.bookings);
      }

      if (this.getBookingsSubs) {
        this.getBookingsSubs.unsubscribe();
      }
    }, err => {
      this.displayAlert();
    });
  }

  getMoreBookings(infiniteScroll) {
    this.page++;
    if (this.page > this.totalPages) {
      console.log("No more bookings");
      infiniteScroll.complete();
    } else {
      console.log("Fetching bookings");
      this.getBookings(infiniteScroll);
    }
  }

  displayAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Sorry!',
      message: 'An unexpected error occured. Please try again later.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    if (this.getBookingsSubs) {
      this.getBookingsSubs.unsubscribe();
    }
    confirm.present();
  }

  getStatusClass(code: string) {
    let result = '';
    switch(code) {
      case 'PENDING':
        result = 'status-pending';
        break;
      case 'APPROVED':
        result = 'status-approved';
        break;
      case 'REJECTED':
      case 'CANCELLED BY ADMIN':
      case 'CANCELLED BY APPLICANT':
        result = 'status-rejected';
        break;
      case 'FULL':
        result = '';
        break;
    }
    return result;
  }

  bookingDetails(booking: Booking) {
    console.log(booking);
    this.navCtrl.push(ApproverViewChemicalsDetailPage, {booking: booking});
  }

  openModal() {
    let filterModal = this.modalCtrl.create(ApproverViewChemicalsFilterPage, { 
      userId: this.userId,
      isFilter: this.isFilter,
      data: {
        status: this.statusIdFilter,
        store: this.storeFilter,
        chemical: this.chemicalFilter,
        nationalId: this.nationalIdFilter,
        startDate: this.startDateFilter,
        endDate: this.endDateFilter
      }
    });
    filterModal.onDidDismiss(response => {
      if (response.search) {
        console.log(response.data);
        this.statusIdFilter = response.data.status;
        this.storeFilter = response.data.store;
        this.chemicalFilter = response.data.chemical;
        this.nationalIdFilter = response.data.nationalId;
        this.startDateFilter = response.data.startDate;
        this.endDateFilter = response.data.endDate;

        if (this.statusIdFilter || this.storeFilter.id || this.chemicalFilter.id || this.nationalIdFilter || this.startDateFilter || this.endDateFilter) {
          this.isFilter = true;
          console.log("Filtering enabled");
        } else {
          this.isFilter = false;
        }

        this.bookings = [];
        this.page = 1;
        this.size = 15;
        this.getBookings();

      }
    });
    filterModal.present();
  }

  translateStatusCode(code: number) {
    let result = '';
    switch(code) {
      case 1:
        result = 'PENDING';
        break;
      case 2:
        result = 'APPROVED';
        break;
      case 3:
        result = 'REJECTED';
        break;
      case 4:
        result = 'FULL';
        break;
      case 5:
        result = 'CANCELLED BY APPLICANT';
        break;
      case 6:
        result = 'CANCELLED BY ADMIN';
        break;
    }
    return result;
  }

}
