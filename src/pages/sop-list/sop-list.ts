import { EbookPage } from './../ebook/ebook';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-sop-list',
  templateUrl: 'sop-list.html',
})
export class SopListPage {

  public items: any;

  searchTerm: string = '';

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public http: HttpClient, private plt: Platform) {

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SopListPage');

    this.getSOP();
  }

  getSOP() {
    let urlmsds = 'https://www.unisza.edu.my/api2/smartlab/sop&searchkey=' + this.searchTerm;
    let datamsds: Observable<any> = this.http.get(urlmsds);
    datamsds.subscribe(result => {
      this.items = result;
    });
  }

  filterProcessing(ev: any) {
    this.getSOP();
    let serVal = ev.target.value;
    if (serVal && serVal.trim() != '') {
      this.items = this.items.filter((item) => {
        return item.NAME.toUpperCase().indexOf(this.searchTerm.toUpperCase()) > -1;
      })
    }
  }

  viewPDF(getID: string, getName: string, getURL: string) {

    var modalPage = this.modalCtrl.create(EbookPage, { id: getID, ebookNAME: getName, ebookURL: getURL });
    modalPage.present();

  }

}
