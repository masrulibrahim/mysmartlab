import { SopListPage } from './../sop-list/sop-list';
import { SopScanPage } from './../sop-scan/sop-scan';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-sop-instrument',
  templateUrl: 'sop-instrument.html',
})
export class SopInstrumentPage {

  tab1Root: any = SopScanPage;
  tab2Root: any = SopListPage;

  myIndex: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.myIndex = navParams.data.tabIndex || 0;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SopInstrumentPage');
  }

}
