import { OshaProcessPage } from './../osha-process/osha-process';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { IonicSelectableComponent } from 'ionic-selectable';

class Lab {
  public LABID: number;
  public LABNAME: string;
}

@Component({
  selector: 'page-osha-report',
  templateUrl: 'osha-report.html',
})
export class OshaReportPage {

  labs: Lab[];
  lab: Lab;

  @ViewChild('ecampus') ecampus;
  @ViewChild('elab') elab;
  @ViewChild('ename') ename;
  @ViewChild('enokp') enokp;
  @ViewChild('enotel') enotel;
  @ViewChild('estatus') estatus;
  @ViewChild('myreport') myreport;
  @ViewChild('etkh') etkh;

  userid: any = '';
  labid: any = '';
  myDate: String = new Date().toISOString();
  myphoto: any;

  public campuses: any = [];
  // public labs: any = [];
  public mytext: string;

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public alertCtrl: AlertController, private camera: Camera, private transfer: FileTransfer, private plt: Platform) {

    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OshaReportPage');
    this.getCampuslist();
  }

  getCampuslist() {

    let urlcampus = 'https://www.unisza.edu.my/api2/smartlab/campuslist';
    let datacampus: Observable<any> = this.http.get(urlcampus);
    datacampus.subscribe(result => {
      this.campuses = result;
    });

  }

  getLablist() {

    let nilai = { ecampus: this.ecampus.value };

    let urlmakmal = 'https://www.unisza.edu.my/api2/smartlab/lablist?campusid=' + nilai.ecampus;
    let datamakmal: Observable<any> = this.http.get(urlmakmal);
    datamakmal.subscribe(result => {
      this.labs = result;
    });

  }

  portChange(event: { component: IonicSelectableComponent, value: any }) {
    this.labid = event.value.LABID;
    console.log('port:', event.value.LABID);
  }

  upload() {

    const options: CameraOptions = {
      quality: 70,
      // destinationType: this.camera.DestinationType.FILE_URI,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:

      this.myphoto = 'data:image/jpeg;base64,' + imageData;

      const fileTransfer: FileTransferObject = this.transfer.create();

      let options1: FileUploadOptions = {
        fileKey: 'file',
        // fileName: this.mytext + '_' + this.myDate + '.jpg',
        fileName: this.userid + '.jpg',
        headers: {}

      }

      fileTransfer.upload(this.myphoto, 'https://portal.unisza.edu.my/corrad_api/ionic_smartlab_upload_osha.php', options1)
        .then((data) => {
          // success
          // alert("success " + JSON.stringify(data));
        }, (err) => {
          // error
          alert("error" + JSON.stringify(err));
        });


    });

  }

  sendReport() {

    let nilai = {

      elab: this.elab.value,
      ename: this.ename.value,
      enokp: this.enokp.value,
      enotel: this.enotel.value,
      estatus: this.estatus.value,
      myreport: this.myreport.value,
      etkh: this.etkh.value

    };

    this.navCtrl.push(OshaProcessPage, { elab: this.labid, ename: nilai.ename, enokp: nilai.enokp, enotel: nilai.enotel, estatus: nilai.estatus, myreport: nilai.myreport, etkh: nilai.etkh });

  }

}
