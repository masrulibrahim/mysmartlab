import { HistoryChemicalPage } from './../history-chemical/history-chemical';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, App, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { IonicSelectableComponent } from 'ionic-selectable';

class Lab {
  public LABID: number;
  public LABNAME: string;
}

class Chem {
  public CHEMID: number;
  public CHEMNAME: string;
}

@Component({
  selector: 'page-book-chemicals-2',
  templateUrl: 'book-chemicals-2.html',
})
export class BookChemicals_2Page {

  labs: Lab[];
  lab: Lab;

  chems: Chem[];
  chem: Chem;

  @ViewChild('ecampus') ecampus;
  @ViewChild('elab') elab;
  @ViewChild('echem') echem;
  @ViewChild('econtent') econtent;
  @ViewChild('epack') epack;

  tkh: any;
  hari: any;
  userid: any = '';
  xoxo: any = '';
  unit: any = '';
  labid: any = '';
  chemid: any = '';

  public campuses: any = [];
  // public labs: any = [];
  // public chems: any = [];
  public packs: any = [];

  plattype: any;

  chemicalsHasSearched = false;

  quantity: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public alertCtrl: AlertController, public appCtrl: App, private plt: Platform) {

    this.userid = localStorage.getItem('storedData');

    this.tkh = this.navParams.get('tkh');
    this.tkh = this.tkh.tahun + '-' + this.tkh.bulan + '-' + this.tkh.hari;

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad BookChemicals_2Page');

    this.getCampuslist();

  }

  getCampuslist() {

    let urlcampus = 'https://www.unisza.edu.my/api2/smartlab/campuslist';
    let datacampus: Observable<any> = this.http.get(urlcampus);
    datacampus.subscribe(result => {
      if (Array.isArray(result)) {
        this.campuses = result;
      }
    });

  }

  getLablist() {

    this.elab.clear();
    this.labs = [];
    this.echem.clear();
    this.chems = [];
    this.chemicalsHasSearched = false;
    this.xoxo = '';
    this.quantity = 0;

    let nilai = {

      ecampus: this.ecampus.value

    };

    let urlmakmal = 'https://www.unisza.edu.my/api2/smartlab/lablist2?campusid=' + nilai.ecampus;
    let datamakmal: Observable<any> = this.http.get(urlmakmal);
    datamakmal.subscribe(result => {
      if (Array.isArray(result)) {
        this.labs = result;
      }
    });

  }

  portChange(event: {component: IonicSelectableComponent, value: any}) {
    
    this.echem.clear();
    this.chems = [];
    this.chemicalsHasSearched = false;
    this.xoxo = '';
    this.quantity = 0;

    this.labid = event.value.LABID;
    console.log('port:', this.labid);
    this.getChemList();

  }

  getChemList() {

    let nilai = {

      ecampus: this.ecampus.value,
      elab: this.elab.value

    };

    let urlchem = 'https://www.unisza.edu.my/api2/smartlab/chemlist?campusid=' + nilai.ecampus + '&labid=' + this.labid;
    let datachem: Observable<any> = this.http.get(urlchem);
    datachem.subscribe(result => {
      if (Array.isArray(result)) {
        this.chems = result;
      }
    });

  }

  portChange2(event: { component: IonicSelectableComponent, value: any }) {
    
    this.chemicalsHasSearched = false;
    this.xoxo = '';
    this.quantity = 0;

    this.chemid = event.value.CHEMID;
    console.log('port:', this.chemid);
    this.getChemPack();

  }

  getChemPack() {

    let urlpack = 'https://www.unisza.edu.my/api2/smartlab/chempack?chemid=' + this.chemid;
    let datapack: Observable<any> = this.http.get(urlpack);
    datapack.subscribe(result => {
      this.chemicalsHasSearched = true;
      if (Array.isArray(result)) {
        this.xoxo = result[0].PACK;
        this.unit = result[0].UNIT;
      }
    });

  }

  backward() {

    this.navCtrl.pop();

  }

  bookingChem() {

    if (!this.quantity || this.quantity <= 0 || this.quantity > parseFloat(this.xoxo)) {
      console.log("Invalid quantity", this.quantity);
      return;
    }

    let urlbook = 'https://www.unisza.edu.my/api2/smartlab/bookingchem?userid=' + this.userid + '&chemid=' + this.chemid + '&bookdate=' + this.tkh + '&portion=' + this.quantity;
    let databook: Observable<any> = this.http.get(urlbook);
    databook.subscribe(result => { });

    let confirm = this.alertCtrl.create({
      title: 'Your chemicals booking will be process...',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.appCtrl.getRootNav().setRoot(HistoryChemicalPage);
          }
        }
      ]
    });
    confirm.present();

  }

  quantityChanged() {
    if (!this.isInt(this.quantity)) {
      this.quantity = null;
    } else if (this.quantity > parseFloat(this.xoxo)) {
      this.quantity = parseFloat(this.xoxo);
    } else {
      this.quantity = parseFloat(this.quantity.toString());
    }
  }

  quantityKeyPressed(event) {
    // console.log(event);
    // 45   -> "-"
    // 46   -> "."
    // 101  -> "e"
    if (event.keyCode === 45 || event.keyCode === 46 || event.keyCode === 101) {
      return false;
    } else {
      this.quantityChanged();
    }
  }

  isInt(value) {
    if (isNaN(value)) {
      return false;
    }
    let x = parseFloat(value);
    return (x | 0) === x;
  }

}
