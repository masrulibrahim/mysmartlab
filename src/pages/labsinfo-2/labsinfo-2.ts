import { LabreviewPage } from './../labreview/labreview';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-labsinfo-2',
  templateUrl: 'labsinfo-2.html',
})
export class Labsinfo_2Page {

  public items: any;

  searchTerm: string = '';

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public modalCtrl: ModalController, private plt: Platform) {

    this.getLablist();

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Labsinfo_2Page');
  }

  getLablist() {
    let urlLab = 'https://www.unisza.edu.my/api2/smartlab/info/lablist?campusid=2&searchkey=' + this.searchTerm;
    let dataLab: Observable<any> = this.http.get(urlLab);
    dataLab.subscribe(result => {
      this.items = result;
    });
  }

  pickStaf(ev: any) {
    this.getLablist();
    let serVal = ev.target.value;
    if (serVal && serVal.trim() != '') {
      this.items = this.items.filter((item) => {
        return item.LABNAME.toUpperCase().indexOf(this.searchTerm.toUpperCase()) > -1;
      })
    }
  }

  viewLab(
    getlabid: string,
    getlabname: string,
    gettypename: string,
    getroomno: string,
    getpeg1: string,
    getpeg2: string,
    getpenpeg1: string,
    getpenpeg2: string,
    getpemmakmal1: string,
    getpemmakmal2: string,
    getlat: string,
    getlng: string,
    getstatus: string,
    getidpeg1: string,
    getidpeg2: string,
    getidpenpeg1: string,
    getidpenpeg2: string,
    getidpemmakmal1: string,
    getidpemmakmal2: string) {

    var modalPage = this.modalCtrl.create(LabreviewPage, { LABID: getlabid });
    modalPage.present();

  }

}
