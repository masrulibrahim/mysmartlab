import { EbookPage } from './../ebook/ebook';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, App, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-msds',
  templateUrl: 'msds.html',
})
export class MsdsPage {

  public items: any;

  searchTerm: string = '';

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public app: App, public http: HttpClient, private plt: Platform) {

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MsdsPage');

    this.getMSDS();
  }

  getMSDS() {
    let urlmsds = 'https://www.unisza.edu.my/api2/smartlab/msds&searchkey=' + this.searchTerm;
    let datamsds: Observable<any> = this.http.get(urlmsds);
    datamsds.subscribe(result => {
      this.items = result;
    });
  }

  filterProcessing(ev: any) {
    this.getMSDS();
    let serVal = ev.target.value;
    if (serVal && serVal.trim() != '') {
      this.items = this.items.filter((item) => {
        return item.NAME.toUpperCase().indexOf(this.searchTerm.toUpperCase()) > -1;
      })
    }
  }

  viewPDF(getID: string, getName: string, getURL: string){

    var modalPage = this.modalCtrl.create(EbookPage, { id: getID, ebookNAME: getName, ebookURL: getURL });
    modalPage.present();

  }

}
