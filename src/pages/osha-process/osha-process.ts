import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, App } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-osha-process',
  templateUrl: 'osha-process.html',
})
export class OshaProcessPage {

  userid: any = '';
  elab: any;
  ename: any;
  enokp: any;
  enotel: any;
  estatus: any;
  myreport: any;
  etkh: any;
  tarikh: any;
  masa: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public alertCtrl: AlertController, public app: App) {

    this.userid = localStorage.getItem('storedData');

    this.elab = this.navParams.get('elab');
    this.ename = this.navParams.get('ename');
    this.enokp = this.navParams.get('enokp');
    this.enotel = this.navParams.get('enotel');
    this.estatus = this.navParams.get('estatus');
    this.myreport = this.navParams.get('myreport');
    this.etkh = this.navParams.get('etkh');

    this.tarikh = this.etkh.year + '-' + this.etkh.month + '-' + this.etkh.day;
    this.masa = this.etkh.hour + ':' + this.etkh.minute + ':' + this.etkh.second;

    // console.log('tarikh: ' + this.tarikh);
    // console.log('masa: ' + this.masa);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OshaProcessPage');
    this.prosesReport();
  }

  prosesReport() {

    let urlosha = 'https://www.unisza.edu.my/api2/smartlab/reportosha?userid=' + this.userid + '&labid=' + this.elab + '&nama=' + this.ename + '&nokp=' + this.enokp + '&notel=' + this.enotel + '&userlevel=' + this.estatus + '&myreport=' + this.myreport + '&tarikh=' + this.tarikh + '&masa=' + this.masa;
    let dataosha: Observable<any> = this.http.get(urlosha);
    dataosha.subscribe(result => { });

    let confirm = this.alertCtrl.create({
      title: 'Your report has been send, Thank You!',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.app.getRootNav().setRoot(HomePage);
          }
        }
      ]
    });
    confirm.present();

  }

}
