import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';

@Component({
  selector: 'page-safetymod1',
  templateUrl: 'safetymod1.html',
})
export class Safetymod1Page {

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private plt: Platform) {

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  closeMe() {
    this.viewCtrl.dismiss();
  }

}
