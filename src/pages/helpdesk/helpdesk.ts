import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-helpdesk',
  templateUrl: 'helpdesk.html',
})
export class HelpdeskPage {

  @ViewChild('message') message;

  userid: any = '';

  public helps: any = [];

  data = { type: '', nickname: '', message: '' };

  plattype: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, private plt: Platform) {

    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpdeskPage');
  }

  sendMessage(){

    let nilai = { message: this.message.value };

    let urlask = 'https://www.unisza.edu.my/api2/smartlab/helpdesk/ask?userid=' + this.userid + '&msg=' + nilai.message;
    let dataask: Observable<any> = this.http.get(urlask);
    dataask.subscribe(result => { });

    this.data.message = '';

    this.ionViewWillEnter();

  }

  ionViewWillEnter() {
    this.myDefaultMethodToFetchData();
  }

  myDefaultMethodToFetchData() {

    let urlhelpdesk = 'https://www.unisza.edu.my/api2/smartlab/helpdesk/get?userid=' + this.userid;
    let datahelpdesk: Observable<any> = this.http.get(urlhelpdesk);
    datahelpdesk.subscribe(result => {
      this.helps = result;
    });

  }

}
