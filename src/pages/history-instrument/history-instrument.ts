import { BookcancellationPage } from './../bookcancellation/bookcancellation';
import { HistoryPage } from './../history/history';
import { ApprovingInstrumentPage } from './../approving-instrument/approving-instrument';
import { HistoryViewsPage } from './../history-views/history-views';
import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController, App } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { ModalController } from 'ionic-angular';

@Component({
  selector: 'page-history-instrument',
  templateUrl: 'history-instrument.html',
})
export class HistoryInstrumentPage {

  userid: any = '';

  public instrumentlist: any = [];

  plattype: any;

  isLoading = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public modalCtrl: ModalController, private plt: Platform, public alertCtrl: AlertController, public appCtrl: App) {

    this.userid = localStorage.getItem('storedData');

    if (this.plt.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.plt.is('iphone')) {
      this.plattype = "iphone";
    }

  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad HistoryInstrumentPage');

    this.getInstrumentlist();

  }

  getInstrumentlist() {
    this.isLoading = true;
    let urlInstlist = 'https://www.unisza.edu.my/api2/smartlab/bookinglist/instruments?userid=' + this.userid;
    let dataInstlist: Observable<any> = this.http.get(urlInstlist);
    dataInstlist.subscribe(result => {
      this.isLoading = false;
      if (Array.isArray(result)) {
        this.instrumentlist = result;
        console.log(this.instrumentlist);
      }
    });

  }

  openModal(getCAT: string, getINSTRUMENT: string, getBARKOD: string, getLOCATION: string, getPEGAWAI: string, getBOOKDATE: string, getAPPLYDATE: string, getSTATUS: string, getNOTE: string) {

    var modalPage = this.modalCtrl.create(HistoryViewsPage, { CAT: getCAT, INSTRUMENT: getINSTRUMENT, BARKOD: getBARKOD, LOCATION: getLOCATION, PEGAWAI: getPEGAWAI, BOOKDATE: getBOOKDATE, APPLYDATE: getAPPLYDATE, STATUS: getSTATUS, NOTE: getNOTE });
    modalPage.present();

  }

  processInstrument(getBOOKID: string) {

    var modalPage = this.modalCtrl.create(ApprovingInstrumentPage, { BOOKID: getBOOKID });
    modalPage.present();

  }

  cancelBook(getBOOKTYPE: string, getBOOKID: string) {
    let confirm = this.alertCtrl.create({
      title: 'Cancel Confirmation',
      message: 'Are you sure you want to cancel this booking?',
      buttons: [
        {
          text: 'Back',
          role: 'cancel'
        },
        {
          text: 'Confirm',
          handler: () => {
            this.navCtrl.push(BookcancellationPage, { BOOKTYPE: getBOOKTYPE, BOOKID: getBOOKID });
          }
        }
      ]
    });
    confirm.present();

  }

}
