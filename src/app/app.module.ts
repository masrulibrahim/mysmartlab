import { Safetymod1Page } from './../pages/safetymod1/safetymod1';
import { EventPage } from './../pages/event/event';
import { QuickmenuPage } from './../pages/quickmenu/quickmenu';
import { HelpdeskRespondPage } from './../pages/helpdesk-respond/helpdesk-respond';
import { HelpdeskAdminPage } from './../pages/helpdesk-admin/helpdesk-admin';
import { HelpdeskPage } from './../pages/helpdesk/helpdesk';
import { ApproverDetailsPage } from './../pages/approver-details/approver-details';
import { ApproverPage } from './../pages/approver/approver';
import { SafetymodPage } from './../pages/safetymod/safetymod';
import { OshaProcessPage } from './../pages/osha-process/osha-process';
import { OshaHistoryPage } from './../pages/osha-history/osha-history';
import { OshaReportPage } from './../pages/osha-report/osha-report';
import { OshaPage } from './../pages/osha/osha';
import { SopScanPage } from './../pages/sop-scan/sop-scan';
import { SopListPage } from './../pages/sop-list/sop-list';
import { SafePipe } from './../pipes/safe/safe';
import { EbookPage } from './../pages/ebook/ebook';
import { PdfViewerPage } from './../pages/pdf-viewer/pdf-viewer';
import { MsdsPage } from './../pages/msds/msds';
import { SopInstrumentPage } from './../pages/sop-instrument/sop-instrument';
import { ComplaintScanPage } from './../pages/complaint-scan/complaint-scan';
import { ComplaintModalPage } from './../pages/complaint-modal/complaint-modal';
import { ComplaintLabPage } from './../pages/complaint-lab/complaint-lab';
import { ComplaintHistoryPage } from './../pages/complaint-history/complaint-history';
import { ComplaintPage } from './../pages/complaint/complaint';
import { BookcancellationPage } from './../pages/bookcancellation/bookcancellation';
import { LabreviewPage } from './../pages/labreview/labreview';
import { Labsinfo_2Page } from './../pages/labsinfo-2/labsinfo-2';
import { Labsinfo_1Page } from './../pages/labsinfo-1/labsinfo-1';
import { ApprovingInstrumentPage } from './../pages/approving-instrument/approving-instrument';
import { ApprovingConsumablePage } from './../pages/approving-consumable/approving-consumable';
import { ApprovingChemicalPage } from './../pages/approving-chemical/approving-chemical';
import { ApprovingLabsPage } from './../pages/approving-labs/approving-labs';
import { RegisterPage } from './../pages/register/register';
import { LabsinfoPage } from './../pages/labsinfo/labsinfo';
import { HistoryViewsPage } from './../pages/history-views/history-views';
import { HistoryLabsPage } from './../pages/history-labs/history-labs';
import { HistoryInstrumentPage } from './../pages/history-instrument/history-instrument';
import { HistoryConsumablePage } from './../pages/history-consumable/history-consumable';
import { HistoryChemicalPage } from './../pages/history-chemical/history-chemical';
import { HistoryPage } from './../pages/history/history';
import { BookInstrument_2Page } from './../pages/book-instrument-2/book-instrument-2';
import { BookConsumable_2Page } from './../pages/book-consumable-2/book-consumable-2';
import { BookChemicals_2Page } from './../pages/book-chemicals-2/book-chemicals-2';
import { BookConsumablePage } from './../pages/book-consumable/book-consumable';
import { ReportPage } from './../pages/report/report';
import { BookLabs_2Page } from './../pages/book-labs-2/book-labs-2';
import { BookChemicalsPage } from './../pages/book-chemicals/book-chemicals';
import { BookLabsPage } from './../pages/book-labs/book-labs';
import { BookPage } from './../pages/book/book';
import { LogoutPage } from './../pages/logout/logout';
import { LoginPage } from './../pages/login/login';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, ToastController } from 'ionic-angular';
import { DirectivesModule } from '../directives/directives.module';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { CalendarModule } from 'ionic3-calendar-en';
import { BookInstrumentPage } from '../pages/book-instrument/book-instrument';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/';
import { Camera } from '@ionic-native/camera';
import { FileTransfer } from '@ionic-native/file-transfer';
import { Geolocation } from '@ionic-native/geolocation';
import { IonicSelectableModule } from 'ionic-selectable';
// import { OneSignal } from '@ionic-native/onesignal';
import { PdfViewerModule } from "ng2-pdf-viewer";
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { BookLabs_3Page } from '../pages/book-labs-3/book-labs-3';
import { CovidGuidelinesPage } from '../pages/covid-guidelines/covid-guidelines';
import { ApproverViewLabsPage } from '../pages/approver-view-labs/approver-view-labs';
import { ApproverViewLabDetailPage } from '../pages/approver-view-lab-detail/approver-view-lab-detail';
import { ApproverViewLabsFilterPage } from '../pages/approver-view-labs-filter/approver-view-labs-filter';
import { ApproverViewChemicalsPage } from '../pages/approver-view-chemicals/approver-view-chemicals';
import { ApproverViewChemicalsDetailPage } from '../pages/approver-view-chemicals-detail/approver-view-chemicals-detail';
import { ApproverViewChemicalsFilterPage } from '../pages/approver-view-chemicals-filter/approver-view-chemicals-filter';
import { ApproverViewConsumablesPage } from '../pages/approver-view-consumables/approver-view-consumables';
import { ApproverViewConsumablesDetailPage } from '../pages/approver-view-consumables-detail/approver-view-consumables-detail';
import { ApproverViewConsumablesFilterPage } from '../pages/approver-view-consumables-filter/approver-view-consumables-filter';
import { ApproverViewInstrumentsPage } from '../pages/approver-view-instruments/approver-view-instruments';
import { ApproverViewInstrumentsDetailPage } from '../pages/approver-view-instruments-detail/approver-view-instruments-detail';
import { ApproverViewInstrumentsFilterPage } from '../pages/approver-view-instruments-filter/approver-view-instruments-filter';

@NgModule({
  declarations: [
    MyApp,
    HomePage, LoginPage, LogoutPage, QuickmenuPage, EventPage,
    BookPage, BookLabsPage, BookLabs_2Page, BookLabs_3Page,
    BookChemicalsPage, BookChemicals_2Page,
    BookConsumablePage, BookConsumable_2Page,
    BookInstrumentPage, BookInstrument_2Page,
    HistoryPage, HistoryChemicalPage, HistoryViewsPage,
    HistoryConsumablePage, HistoryInstrumentPage, HistoryLabsPage,
    ReportPage, ComplaintPage, ComplaintHistoryPage, ComplaintLabPage, ComplaintModalPage, ComplaintScanPage,
    OshaPage, OshaReportPage, OshaHistoryPage, OshaProcessPage,
    LabsinfoPage, RegisterPage,
    ApprovingLabsPage, ApprovingChemicalPage, ApprovingConsumablePage, ApprovingInstrumentPage,
    BookcancellationPage,
    Labsinfo_1Page, Labsinfo_2Page, LabreviewPage,
    SopInstrumentPage, SopListPage, SopScanPage,
    MsdsPage,
    SafetymodPage, Safetymod1Page,
    ApproverPage, ApproverDetailsPage,
    PdfViewerPage, EbookPage,
    HelpdeskPage, HelpdeskAdminPage, HelpdeskRespondPage,
    SafePipe,
    CovidGuidelinesPage, PdfViewerPage,
    ApproverViewLabsPage, ApproverViewLabDetailPage, ApproverViewLabsFilterPage,
    ApproverViewChemicalsPage, ApproverViewChemicalsDetailPage, ApproverViewChemicalsFilterPage,
    ApproverViewConsumablesPage, ApproverViewConsumablesDetailPage, ApproverViewConsumablesFilterPage,
    ApproverViewInstrumentsPage, ApproverViewInstrumentsDetailPage, ApproverViewInstrumentsFilterPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    CalendarModule,
    DirectivesModule,
    IonicSelectableModule,
    PdfViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage, LoginPage, LogoutPage, QuickmenuPage, EventPage,
    BookPage, BookLabsPage, BookLabs_2Page, BookLabs_3Page,
    BookChemicalsPage, BookChemicals_2Page,
    BookConsumablePage, BookConsumable_2Page,
    BookInstrumentPage, BookInstrument_2Page,
    HistoryPage, HistoryChemicalPage, HistoryViewsPage,
    HistoryConsumablePage, HistoryInstrumentPage, HistoryLabsPage,
    ReportPage, ComplaintPage, ComplaintHistoryPage, ComplaintLabPage, ComplaintModalPage, ComplaintScanPage,
    OshaPage, OshaReportPage, OshaHistoryPage, OshaProcessPage,
    LabsinfoPage, RegisterPage,
    ApprovingLabsPage, ApprovingChemicalPage, ApprovingConsumablePage, ApprovingInstrumentPage,
    BookcancellationPage,
    Labsinfo_1Page, Labsinfo_2Page, LabreviewPage,
    SopInstrumentPage, SopListPage, SopScanPage,
    MsdsPage,
    SafetymodPage, Safetymod1Page,
    ApproverPage, ApproverDetailsPage,
    HelpdeskPage, HelpdeskAdminPage, HelpdeskRespondPage,
    PdfViewerPage, EbookPage,
    CovidGuidelinesPage, PdfViewerPage,
    ApproverViewLabsPage, ApproverViewLabDetailPage, ApproverViewLabsFilterPage,
    ApproverViewChemicalsPage, ApproverViewChemicalsDetailPage, ApproverViewChemicalsFilterPage,
    ApproverViewConsumablesPage, ApproverViewConsumablesDetailPage, ApproverViewConsumablesFilterPage,
    ApproverViewInstrumentsPage, ApproverViewInstrumentsDetailPage, ApproverViewInstrumentsFilterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ToastController,
    BarcodeScanner,
    Camera,
    FileTransfer,
    Geolocation,
    LaunchNavigator,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
