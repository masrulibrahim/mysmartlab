import { HelpdeskAdminPage } from './../pages/helpdesk-admin/helpdesk-admin';
import { HelpdeskPage } from './../pages/helpdesk/helpdesk';
import { ApproverPage } from './../pages/approver/approver';
import { SafetymodPage } from './../pages/safetymod/safetymod';
import { RegisterPage } from './../pages/register/register';
import { OshaPage } from './../pages/osha/osha';
import { SopInstrumentPage } from './../pages/sop-instrument/sop-instrument';
import { MsdsPage } from './../pages/msds/msds';
import { HomePage } from './../pages/home/home';
import { ComplaintPage } from './../pages/complaint/complaint';
import { LabsinfoPage } from './../pages/labsinfo/labsinfo';
import { HistoryPage } from './../pages/history/history';
import { BookPage } from './../pages/book/book';
import { LogoutPage } from './../pages/logout/logout';
import { LoginPage } from './../pages/login/login';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
// import { OneSignal, OSNotificationPayload } from '@ionic-native/onesignal';
// import { isCordovaAvailable } from '../common/is-cordova-avaliable';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  userid: any = '';
  namauser: any;
  iduser: any;
  rank: any;

  userlevel: Observable<any>;
  userinfo: Observable<any>;

  rootPage: any = HomePage;

  pages: Array<{ title: string, component: any, icon: string; }>;

  plattype: any;

  // constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public httpClient: HttpClient, private oneSignal: OneSignal) {
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public httpClient: HttpClient) {
    this.initializeApp();

    if (this.platform.is('ipad')) {
      this.plattype = "ipad";
    } else if (this.platform.is('iphone')) {
      this.plattype = "iphone";
    }

    this.userid = localStorage.getItem('storedData');
    console.log('userid@menu:', this.userid);

    this.userLevel();

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      // if (isCordovaAvailable()) {
      //   this.oneSignal.startInit('08cca8dd-3a70-4782-93a1-1cefb6ef7e28', '114130869367');
      //   this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
      //   this.oneSignal.handleNotificationReceived().subscribe(data => this.onPushReceived(data.payload));
      //   this.oneSignal.handleNotificationOpened().subscribe(data => this.onPushOpened(data.notification.payload));
      //   this.oneSignal.endInit();
      // }


      statusBar.styleDefault();
      splashScreen.hide();
    });

  }

  // private onPushReceived(payload: OSNotificationPayload) {
  //   alert('Push recevied:' + payload.body);
  // }

  // private onPushOpened(payload: OSNotificationPayload) {
  //   alert('Push opened: ' + payload.body);
  // }

  userLevel() {

    this.userlevel = this.httpClient.get('https://www.unisza.edu.my/api2/smartlab/userlevel?userid=' + this.userid);
    this.userlevel.subscribe(data => {
      // console.log('userlevel: ', data);
      if (this.userid) {
        this.rank = data[0]['KAT'];
      } else {
        this.rank = "ANONYMOUS";
      }
      console.log('userlevel: ', this.rank);
      this.sideMenu(data);
    })

  }

  userInfo() {

    this.userinfo = this.httpClient.get('https://www.unisza.edu.my/api2/smartlab/introinfo?userid=' + this.userid);
    this.userinfo.subscribe(userdata => {

      if (this.userid) {

        console.log('nama: ', userdata[0]['NAMA']);
        this.namauser = userdata[0]['NAMA'];
        this.iduser = userdata[0]['ID'];

      } else {

        this.namauser = "WELCOME";
        this.iduser = "UniSZA smartLAB";

      }
    })

  }

  sideMenu(data) {

    // let category = data.category;
    let category = data[0]['KAT'];
    // let adminstatus = data.adminstatus;
    let adminstatus = data[0]['ADMINSTATUS'];
    console.log('category@menu:', category);

    if (category == "1") {

      if (adminstatus == "1"){

        this.pages = [
          { title: 'Home', component: HomePage, icon: 'home' },
          { title: 'Booking', component: BookPage, icon: 'custom-booking' },
          { title: 'History', component: HistoryPage, icon: 'custom-data' },
          { title: 'Labs Review', component: LabsinfoPage, icon: 'custom-lab' },
          { title: 'Complaint', component: ComplaintPage, icon: 'custom-complaint' },
          { title: 'OSHA/OSHE Report', component: OshaPage, icon: 'medical' },
          { title: 'MSDS', component: MsdsPage, icon: 'custom-msds' },
          { title: 'Instrument SOP', component: SopInstrumentPage, icon: 'custom-sop' },
          { title: 'Safety Module', component: SafetymodPage, icon: 'custom-safetymod' },
          { title: 'Ext. User Approval', component: ApproverPage, icon: 'custom-external-user' },
          { title: 'Helpdesk', component: HelpdeskAdminPage, icon: 'custom-helpdesk' },
          { title: 'Logout', component: LogoutPage, icon: 'custom-exit' }
        ];

      }else{

        this.pages = [
          { title: 'Home', component: HomePage, icon: 'home' },
          { title: 'Booking', component: BookPage, icon: 'custom-booking' },
          { title: 'History', component: HistoryPage, icon: 'custom-data' },
          { title: 'Labs Review', component: LabsinfoPage, icon: 'custom-lab' },
          { title: 'Complaint', component: ComplaintPage, icon: 'custom-complaint' },
          { title: 'OSHA/OSHE Report', component: OshaPage, icon: 'medical' },
          { title: 'MSDS', component: MsdsPage, icon: 'custom-msds' },
          { title: 'Instrument SOP', component: SopInstrumentPage, icon: 'custom-sop' },
          { title: 'Safety Module', component: SafetymodPage, icon: 'custom-safetymod' },
          { title: 'Helpdesk', component: HelpdeskPage, icon: 'custom-helpdesk' },
          { title: 'Logout', component: LogoutPage, icon: 'custom-exit' }
        ];

      }

    }else{

      this.pages = [
        { title: 'Home', component: HomePage, icon: 'home' },
        { title: 'Labs Review', component: LabsinfoPage, icon: 'custom-lab' },
        { title: 'MSDS', component: MsdsPage, icon: 'custom-msds' },
        { title: 'Instrument SOP', component: SopInstrumentPage, icon: 'custom-sop' },
        { title: 'Safety Module', component: SafetymodPage, icon: 'custom-safetymod' },
        { title: 'Register', component: RegisterPage, icon: 'custom-add-user' },
        { title: 'Login', component: LoginPage, icon: 'custom-login' }
      ];

    }

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
}
